package com.lod.myapplication_user.UtilsAndUrls;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.lod.myapplication_user.ProgressDialogClass;
import com.lod.myapplication_user.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Admin on 12/18/2017.
 */

public class WalletAmountForCustomer {


    public static String msg = "";
    public static boolean error = false;

    public static void getWalletAmount(final Context context ) {
        AndroidNetworking.get(ConfigURL.URL_GET_WALLET)
                .addQueryParameter("pMobile", ConfigURL.getMobileNumber(context))
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressDialogClass.dismissDottedProgress();
                        try {
                            msg =""+ response.getString("wallet");
                            error = response.getBoolean("error");
                            Log.v("RS", msg);
                        } catch (JSONException e) {
                            Log.e("RS", e.getMessage());
                            e.printStackTrace();

                        }
                        Log.v("RS", msg);
                       // if(!error)
                        TextView txtView = (TextView) ((Activity)context).findViewById(R.id.tv_wallet_category);
                        if(msg.equals("")) {
                            msg="0";
                            txtView.setText("Rs." + msg);
                        }
                        else
                            {
                                txtView.setText("Rs." + msg);
                            }


                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("RS", "" + anError);
                    }
                });
    }
}
