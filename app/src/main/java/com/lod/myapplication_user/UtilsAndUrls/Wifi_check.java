package com.lod.myapplication_user.UtilsAndUrls;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by nafees on 11/14/2017.
 */

public class Wifi_check {

    Context context;

    public Wifi_check(Context context){
        this.context= context;
    }

    //for Internet connection checking
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)   context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
