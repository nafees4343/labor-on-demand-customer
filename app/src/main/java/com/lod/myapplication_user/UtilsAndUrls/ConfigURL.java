package com.lod.myapplication_user.UtilsAndUrls;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.lod.myapplication_user.ProgressDialogClass;

import org.json.JSONException;
import org.json.JSONObject;

import static android.content.ContentValues.TAG;

/**
 * Created by nfs on 8/8/2016.
 */
public class ConfigURL {

    public static final String ip = "www.itehadmotors.com";
    Context context;
    static Boolean isError = false;
    public static final String URL_REGISTER = "http://"+ip+"/qasimWork/laborondemandwebservice/v1/customer";
    public static final String URL_ISPERSONEXIST = "http://"+ip+"/LabourOnDemand/v1/isPersonExist";
    public static final String URL_LOGINCUSTOMER = "http://"+ip+"/qasimWork/laborondemandwebservice/v1/login/customer";
    public static final String URL_FORGOTPASSWORD = "http://"+ip+"/LabourOnDemand/v1/forgot";
    public static final String URL_GET_PERSON_PROFILE = "http://"+ip+"/qasimWork/laborondemandwebservice/v1/customerProfile/";
    public static final String URL_GET_PERSON_ORDERS = "http://"+ip+"/qasimWork/laborondemandwebservice/v1/customerJobs/";
    public static final String URL_PROFILE_CHANGE_PASSWORD = "http://"+ip+"/qasimWork/laborondemandwebservice/v1/customer/changePassword";
    public static final String URL_GET_WALLET = "http://"+ip+"/qasimWork/laborondemandwebservice/v1/customer/wallet/";
    public static final String URL_GET_CATEGORY = "http://"+ip+"/qasimWork/laborondemandwebservice/v1/category";
    public static final String URL_BOOKNOW = "http://"+ip+"/qasimWork/laborondemandwebservice/v1/bookNow";
    public static final String URL_GET_WORKER_CURRENT_LOCATION = "http://"+ip+"/qasimWork/laborondemandwebservice/v1/workerlocation/";
    public static final String URL_GET_TRANSACTION ="http://"+ip+"/qasimWork/laborondemandwebservice/v1/customer/transaction/";
    public static final String URL_SEND_RATING_BY_CUSTOMER ="http://"+ip+"/qasimWork/laborondemandwebservice/v1/sendrattingsbycustomer";
    public static final String URL_CUSTOMER_JOB_STATUS ="http://"+ip+"/qasimWork/laborondemandwebservice/v1/customerJobstatus";
    public static final String URL_POST_JOB_CANCEL = "http://"+ip+"/qasimWork/laborondemandwebservice/v1/customer/jobCancel";
    public static final String URL_POST_BOOK_LATER = "http://"+ip+"/qasimWork/laborondemandwebservice/v1/createbooklater";
    public static final String URL_GET_SCHEDULEJOBS_CUSTOMERS = "http://"+ip+"/qasimWork/laborondemandwebservice/v1/customerschedulejobs";
    public static final String URL_CONFIRM_JOB_AMOUNT = "http://"+ip+"/qasimWork/laborondemandwebservice/v1/jobamountconfirmation";
    public static final String URL_CANCEL_JOB_SHEDULE = "http://"+ip+"/qasimWork/laborondemandwebservice/v1/cancelschedulejob";





    public static String getLoginState(Context context)
    {
        SharedPreferences prefs =   context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if(prefs.getString("LOGIN","").length() >0 )
        {
            return  prefs.getString("LOGIN","");
        }
        else
            return "";
    }

    public static String getMobileNumber(Context context)
    {
        SharedPreferences prefs =   context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if(prefs.getString("PHONE","").length() >0)
        {
            return  prefs.getString("PHONE","");
        }
        else
            return "";
    }

    public static String getWorkerName(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("NAME", "").length() > 0) {
            return prefs.getString("NAME", "");
        } else
            return "";
    }

    public static String getWorkerEmail(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("EMAIL", "").length() > 0) {
            return prefs.getString("EMAIL", "");
        } else
            return "";
    }
    public static String getCategoryid(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("categoryid", "").length() > 0) {
            return prefs.getString("categoryid", "");
        } else
            return "";
}

    public static void save_location_address_(Context context ,String address,String lat,String lng,String catId) {

        SharedPreferences preferences = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("address",address+"");
        editor.putString("lat",lat+"");
        editor.putString("lng",lng+"");
        editor.putString("categoryid",catId+"");
        editor.commit();
    }

    public static String getAddress(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("address", "").length() > 0) {
            return prefs.getString("address", "");
        } else
            return "";
    }

    public static String getlat(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("lat", "").length() > 0) {
            return prefs.getString("lat", "");
        } else
            return "";
    }

    public static String getlng(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("lng", "").length() > 0) {
            return prefs.getString("lng", "");
        } else
            return "";
    }

    public static String getjobId(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("jobId", "").length() > 0) {
            return prefs.getString("jobId", "");
        } else
            return "";
    }

    public static boolean JobCancel(Context context, String jobID, String reasonId) {

        ProgressDialogClass.showDottedProgress(context,"please wait...");
        AndroidNetworking.post(ConfigURL.URL_POST_JOB_CANCEL)
                .addBodyParameter("reasonId", ""+reasonId)
                .addBodyParameter("jobId", jobID)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            ProgressDialogClass.dismissDottedProgress();
                            response.getString("message");
                            if (response.getBoolean("error")) {
                                isError = true;
                                Log.v(TAG, "Job Cancel NOT DONE");
//                                Toast.makeText(context, "Finish Fail", Toast.LENGTH_SHORT).show();
                            } else {
                                isError = false;
                                Log.v(TAG, "Cancel Job DONE");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError error) {
                        ProgressDialogClass.dismissDottedProgress();
                    }
                });
        return isError;
    }

    public static String get_confirm_amount_param(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("confirm_amount", "").length() > 0) {
            return prefs.getString("confirm_amount", "");
        } else
            return "";
    }

    public static void clearshareprefrence(Context context){
        SharedPreferences preferences = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();

    }

  /*  public static String getApiKey(Context context)
    {
        SharedPreferences prefs =   context.getSharedPreferences(
                "PREFRENCE", Context.MODE_PRIVATE);
        if(prefs.getString("API_KEY","").length() >0)
        {
            return  prefs.getString("API_KEY","");
        }
        else
            return "";
    }*/

   /* public static String getEmailId(Context context)
    {
        SharedPreferences prefs =   context.getSharedPreferences(
                "PREFRENCE", Context.MODE_PRIVATE);
        if(prefs.getString("API_KEY","").length() >0)
        {
            return  prefs.getString("email_id","");
        }
        else
            return "";
    }*/


   /* public static void clearshareprefrence(Context context){
        SharedPreferences preferences = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }*/

}
