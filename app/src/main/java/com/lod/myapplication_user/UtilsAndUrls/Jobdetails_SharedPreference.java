package com.lod.myapplication_user.UtilsAndUrls;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Admin on 2/7/2018.
 */

public class Jobdetails_SharedPreference {

    public static String getlatitude(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("latitude", "").length() > 0) {
            return prefs.getString("latitude", "");
        } else
            return "";
    }

    public static String getlongitude(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("longitude", "").length() > 0) {
            return prefs.getString("longitude", "");
        } else
            return "";
    }
    public static String getstatus(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("state", "").length() > 0) {
            return prefs.getString("state", "");
        } else
            return "";
    }

    public static String getworkerName(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("workerName", "").length() > 0) {
            return prefs.getString("workerName", "");
        } else
            return "";
    }

    public static String getworkerImage(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("workerImage", "").length() > 0) {
            return prefs.getString("workerImage", "");
        } else
            return "";
    }

    public static String getworkerPhoneNo(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("workerPhoneNo", "").length() > 0) {
            return prefs.getString("workerPhoneNo", "");
        } else
            return "";
    }

    public static String getjobStatus(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("jobStatus", "").length() > 0) {
            return prefs.getString("jobStatus", "");
        } else
            return "";
    }

    public static String getjobTitle(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("jobTitle", "").length() > 0) {
            return prefs.getString("jobTitle", "");
        } else
            return "";
    }

    public static String getjobId(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("jobId", "").length() > 0) {
            return prefs.getString("jobId", "");
        } else
            return "";
    }

    public static String getjobCost(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("jobCost", "").length() > 0) {
            return prefs.getString("jobCost", "");
        } else
            return "";
    }


    public static String getcustomerPay(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("customerPay", "").length() > 0) {
            return prefs.getString("customerPay", "");
        } else
            return "";
    }

    public static String gettype(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("type", "").length() > 0) {
            return prefs.getString("type", "");
        } else
            return "";
    }


    public static void save_jobCost(Context context ,String jobCost) {

        SharedPreferences preferences = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("jobCost", jobCost+"");
        editor.commit();
    }

    public static void get_work_details_in_shared_preference(Context context,String latitude, String longitude, String workerName, String workerImage, String workerPhoneNo, String jobStatus, String jobTitle, String jobId,String jobCost,String customerPay,String type) {


        SharedPreferences preferences = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("latitude", latitude+"");
        editor.putString("longitude", longitude+"");
        editor.putString("workerName", workerName+"");
        editor.putString("workerImage", workerImage+"");
        editor.putString("workerPhoneNo", workerPhoneNo+"");
        editor.putString("jobStatus", jobStatus+"");
        editor.putString("jobTitle", jobTitle+"");
        editor.putString("jobId", jobId+"");
        editor.putString("jobCost", jobCost+"");
        editor.putString("customerPay", customerPay+"");
        editor.putString("type", type+"");
        editor.commit();
    }


    public static void set_type_in_shared_preference(Context context , String type) {

        SharedPreferences preferences = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("type", type+"");
        editor.commit();
    }

    public static void set_Job_cost_in_shared_preference(Context context , String jobCost) {

        SharedPreferences preferences = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("jobCost", jobCost+"");
        editor.commit();
    }

    public static void set_customer_Pay_in_shared_preference(Context context , String customerPay) {

        SharedPreferences preferences = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("customerPay", customerPay+"");
        editor.commit();
    }






}
