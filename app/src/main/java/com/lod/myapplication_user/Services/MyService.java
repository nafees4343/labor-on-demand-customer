package com.lod.myapplication_user.Services;


import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.lod.myapplication_user.UtilsAndUrls.ConfigURL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

public class MyService extends Service {

    String workerId;
    Double latitudee,longitudee;
    String latitude,longitude;
    String personType;
    String WorkerPhoneno;

    public final static String MY_ACTION = "MY_ACTION";

    public  int notify = 3000;  //interval between two services(Here Service run every 5 Minute)
    private Handler mHandler = new Handler();   //run on another Thread to avoid crash
    private Timer mTimer = null;    //timer handling

    @Override
    public IBinder onBind(Intent intent) {
         WorkerPhoneno = intent.getStringExtra("phoneNo");
        Log.d("SERVICE",WorkerPhoneno);
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        if (mTimer != null) // Cancel if already existed
            mTimer.cancel();
        else
            mTimer = new Timer();   //recreate new
        mTimer.scheduleAtFixedRate(new TimeDisplay(), 0, notify);   //Schedule task
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTimer.cancel();    //For Cancel Timer
        //Toast.makeText(this, "Service is Destroyed", Toast.LENGTH_SHORT).show();
    }

    //class TimeDisplay for handling task
    class TimeDisplay extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    // display toast
                    //Toast.makeText(MyService.this, "Service is running", Toast.LENGTH_SHORT).show();
                    update_Latlng();
                    Intent intent = new Intent();
                    intent.setAction(MY_ACTION);
                    intent.putExtra("latitudee", latitudee);
                    intent.putExtra("longitudee", longitudee);
                    sendBroadcast(intent);
                }
            });
        }
    }

    public void update_Latlng() {

        AndroidNetworking.get(ConfigURL.URL_GET_WORKER_CURRENT_LOCATION)
                .addQueryParameter("personMobileNo",WorkerPhoneno)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        //Toast.makeText(MyService.this, ""+response, Toast.LENGTH_SHORT).show();

                        try {
                            JSONArray jsonArray = response.getJSONArray("worker");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject jsonobject = jsonArray.getJSONObject(i);
                                workerId = jsonobject.getString("maxId");
                                latitude = jsonobject.getString("latitude");
                                longitude = jsonobject.getString("longitude");
                            }
                            //Toast.makeText(MyService.this, "Service is ssd"+"lat: "+latitude+"long: "+longitude, Toast.LENGTH_SHORT).show();
                            latitudee = Double.parseDouble(latitude);
                            longitudee = Double.parseDouble(longitude);
                            //Toast.makeText(MyService.this,""+"latitude: "+latitudee+"longitude: "+longitudee, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        //Toast.makeText(MyService.this, "" + anError, Toast.LENGTH_LONG).show();
                    }
                });
    }
}