package com.lod.myapplication_user.ViewProfile;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.lod.myapplication_user.R;
import com.lod.myapplication_user.UtilsAndUrls.ConfigURL;
import com.lod.myapplication_user.UtilsAndUrls.Wifi_check;


public class Profile extends AppCompatActivity implements View.OnClickListener {

    TextView tv_rate_app, tv_consumer_email_id, tv_consumer_mobile_no, tv_consumer_name;
    Button bt_consumer_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Toolbar toolbar = findViewById(R.id.toolbar_settings);
        toolbar.setTitle("Profile");
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                onBackPressed();

            }
        });

        //Register All Views
        register();
        //Call Clicklisteners
        clicklisteners();


        if (new Wifi_check(this).isNetworkAvailable()) {
            //get_profile_data();
            tv_consumer_name.setText(ConfigURL.getWorkerName(Profile.this));
            tv_consumer_email_id.setText(ConfigURL.getWorkerEmail(Profile.this));
            tv_consumer_mobile_no.setText(ConfigURL.getMobileNumber(Profile.this));
        } else {
            Snackbar.make(findViewById(android.R.id.content), "Internet not connected", Snackbar.LENGTH_SHORT).show();
        }

    }

    void clicklisteners() {
        bt_consumer_password.setOnClickListener(this);
    }

    void register() {
        bt_consumer_password = (Button) findViewById(R.id.tv_customer_password);
        tv_consumer_name = (TextView) findViewById(R.id.tv_customer_name);
        tv_consumer_email_id = (TextView) findViewById(R.id.tv_customer_email_id);
        tv_consumer_mobile_no = (TextView) findViewById(R.id.tv_customer_phone_no);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_customer_password:
                Intent i = new Intent(Profile.this, Password_change.class);
                startActivity(i);
                break;
        }
    }

    }

