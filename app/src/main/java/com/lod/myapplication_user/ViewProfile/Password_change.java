package com.lod.myapplication_user.ViewProfile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.lod.myapplication_user.Category.CategoryActivity;
import com.lod.myapplication_user.ProgressDialogClass;
import com.lod.myapplication_user.R;
import com.lod.myapplication_user.UtilsAndUrls.ConfigURL;
import com.lod.myapplication_user.UtilsAndUrls.Wifi_check;

import org.json.JSONException;
import org.json.JSONObject;

public class Password_change extends AppCompatActivity implements View.OnClickListener {

    Button btn_done;
    EditText et_old_password, et_new_password, et_new_confirm_password;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        //LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        setContentView(R.layout.activity_password_change);


        //inflate your activity layout here!
       /* View contentView = inflater.inflate(R.layout.activity_password_change, null, false);
        drawer.addView(contentView, 1);
*/

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_passchange);
        toolbar.setTitle("Change Password");
        setSupportActionBar(toolbar);


       /* ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();*/

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                onBackPressed();
            }
        });


        btn_done = (Button) findViewById(R.id.bt_save);
        et_old_password = (EditText) findViewById(R.id.et_current_password);
        et_new_password = (EditText) findViewById(R.id.et_new_password);
        et_new_confirm_password = (EditText) findViewById(R.id.et_new_passwordConfirmation);

        btn_done.setOnClickListener(this);


    }

    public void sendData() {

        AndroidNetworking.post(ConfigURL.URL_PROFILE_CHANGE_PASSWORD)
                .addBodyParameter("pMobile", ConfigURL.getMobileNumber(this))
                .addBodyParameter("oldPassword", et_old_password.getText().toString())
                .addBodyParameter("newPassword", et_new_password.getText().toString())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressDialogClass.dismissDottedProgress();
                        String msg = "";
                        boolean error = false;
                        try {
                            msg = response.getString("message");
                            error = response.getBoolean("error");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(Password_change.this, "" + msg, Toast.LENGTH_LONG).show();
                        if(!error) {
                            Intent i = new Intent(Password_change.this, CategoryActivity.class);
                            startActivity(i);
                            finish();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        ProgressDialogClass.dismissDottedProgress();
                        Toast.makeText(Password_change.this, "" + anError, Toast.LENGTH_LONG).show();

                    }
                });

    }

    public void submit() {

        if (et_old_password.getText().toString().isEmpty()) {
            et_old_password.setError("Old Password Cannot Be Empty");
            requestFocus(et_old_password);
            return;
        }
        if (et_new_password.getText().toString().isEmpty()) {
            et_new_password.setError("New Password Cannot Be Empty");
            requestFocus(et_new_password);
            return;
        }
        if (et_new_confirm_password.getText().toString().isEmpty()) {
            et_new_confirm_password.setError("Confirm Password Cannot Be Empty");
            requestFocus(et_new_confirm_password);
            return;
        }
        if(!et_new_password.getText().toString().equals(et_new_confirm_password.getText().toString()))
        {
            et_new_password.setError("Passwords are not matching");
            requestFocus(et_new_confirm_password);
            return;
        }

        if (new Wifi_check(this).isNetworkAvailable()) {
            ProgressDialogClass.showDottedProgress(this,"Please wait...");
            sendData();
        } else {
            ProgressDialogClass.dismissDottedProgress();
            Snackbar.make(findViewById(android.R.id.content), "Internet Not Connected",
                    Snackbar.LENGTH_SHORT).show();
        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_save:
                submit();

                break;
        }
    }


}