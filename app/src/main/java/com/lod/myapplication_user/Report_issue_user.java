package com.lod.myapplication_user;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.lod.myapplication_user.UtilsAndUrls.KeyboardUtil;

public class Report_issue_user extends AppCompatActivity {

    TextView tv_input_message,tv_input_subject;
    Button btn_send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_issue_user);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_report_issue);
        toolbar.setTitle("Report Issue");
        setSupportActionBar(toolbar);


        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                onBackPressed();

            }
        });

        tv_input_message = (TextView) findViewById(R.id.input_message);
        tv_input_subject = (TextView) findViewById(R.id.input_subject);
        btn_send = (Button) findViewById(R.id.btn_snd);

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                composeEmail(tv_input_message.getText().toString(),tv_input_subject.getText().toString());
            }
        });


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

    }

    public void composeEmail(String message, String subject) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
            "mailto","nafees4343@gmail.com", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, message);
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
        }
    }
