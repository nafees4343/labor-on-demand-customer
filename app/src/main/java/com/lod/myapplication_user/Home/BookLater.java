package com.lod.myapplication_user.Home;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.lod.myapplication_user.Category.CategoryActivity;
import com.lod.myapplication_user.Current_worker;
import com.lod.myapplication_user.Login.LoginActivity;
import com.lod.myapplication_user.Models.Customer_profile;
import com.lod.myapplication_user.Nav_drawer_user;
import com.lod.myapplication_user.Notifications.MainActivity;
import com.lod.myapplication_user.ProgressDialogClass;
import com.lod.myapplication_user.R;
import com.lod.myapplication_user.UtilsAndUrls.ConfigURL;
import com.lod.myapplication_user.UtilsAndUrls.Jobdetails_SharedPreference;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * Created by nafees ahmed on 10/10/2017.
 */
public class BookLater extends AppCompatActivity implements View.OnClickListener,TimePickerDialog.OnTimeSetListener,DatePickerDialog.OnDateSetListener {


    Context context;
    Button et_dayOfWork,et_timeOfWork;
    Dialog dialog;
    String date_send="",current_date,widget_date,current_hour,timepicker_hour,time,date,TAG;
    long now;
    Button bt_bookLaterDone;
    TextView tv_time,tv_date,tv_address_booklater;
    DateHelper widgetdate,currentdate;
    int HourOfDay,  Minute,  Second , Year ,MonthOfYear ,DayOfMonth;
    TimePicker tpd;
    int interval = 1;
    Intent i;
    public BookLater(){
        context=this;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_later);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_booklater);

        toolbar.setTitle("Book Later");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                onBackPressed();

            }
        });

        Calendar calendar = Calendar.getInstance();
        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);




        Log.d("Location_address","location: "+ ConfigURL.getAddress(BookLater.this)+"latitude:  "+ConfigURL.getlat(BookLater.this)+"longitude: "+ConfigURL.getlng(BookLater.this)+"hour: : "+hourOfDay+"Mins: "+minutes);

        Registerviews();

        tv_address_booklater.setText(ConfigURL.getAddress(BookLater.this));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.et_dayOfWork:
                Calendar now = Calendar.getInstance();
                Log.d("HS3: ",""+(now.get(Calendar.HOUR_OF_DAY)+3));
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        BookLater.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                Calendar date0 = Calendar.getInstance();
                Calendar date1 = Calendar.getInstance();
                Calendar date2 = Calendar.getInstance();

                if((now.get(Calendar.HOUR_OF_DAY)+interval)>=24){
                    date0.add(Calendar.DAY_OF_MONTH, +1);
                    date1.add(Calendar.DAY_OF_MONTH, +2);
                    date2.add(Calendar.DAY_OF_MONTH, +3);
                    now.add(Calendar.DAY_OF_MONTH, 1);
                    dpd.setMinDate(now);
                }
                else {
                    date0.add(Calendar.DAY_OF_MONTH, +0);
                    date1.add(Calendar.DAY_OF_MONTH, +1);
                    date2.add(Calendar.DAY_OF_MONTH, +2);
                    dpd.setMinDate(now);

                }

                Calendar[] days = {date0,date1,date2};
                dpd.setHighlightedDays(days);
                dpd.setMaxDate(date2);
                dpd.show(getFragmentManager(), "Datepickerdialog");

                break;

            case R.id.et_timeOfWork:
                String dateee="";
                dateee = et_dayOfWork.getText().toString();
                if(!dateee.equals("")) {

                    now = Calendar.getInstance();
                    widgetdate = new DateHelper(Year, MonthOfYear, DayOfMonth);
                    currentdate = new DateHelper(Calendar.getInstance().get(Calendar.YEAR), (Calendar.getInstance().get(Calendar.MONTH)), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

                    try {
                        current_date = currentdate.getMonthAndDay();
                        widget_date = widgetdate.getMonthAndDay();
                        Log.d("DDD", "cr : " + current_date + "\ndate picker: " + widget_date);
                        if (current_date.equals(widget_date)) {

                            TimePickerDialog tpd = TimePickerDialog.newInstance(
                                    BookLater.this,
                                    now.get(Calendar.HOUR_OF_DAY),
                                    now.get(Calendar.MINUTE),
                                    now.get(Calendar.SECOND),
                                    false

                            );
                            // Log.d("tp","Time: "+now.get(Calendar.HOUR_OF_DAY)+now.get(Calendar.MINUTE)+now.get(Calendar.SECOND));
                            tpd.setMinTime(now.get(Calendar.HOUR_OF_DAY) + interval, now.get(Calendar.MINUTE), now.get(Calendar.SECOND));
                            tpd.show(getFragmentManager(), "Timepickerdialog");
                        } else {

                            TimePickerDialog tpd = TimePickerDialog.newInstance(
                                    BookLater.this,
                                    now.get(Calendar.HOUR_OF_DAY),
                                    now.get(Calendar.MINUTE),
                                    now.get(Calendar.SECOND),
                                    false

                            );
                            tpd.is24HourMode();
                            tpd.show(getFragmentManager(), "Timepickerdialog");

                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    Toast.makeText(this, "first set the date", Toast.LENGTH_SHORT).show();
                }



                break;
            case R.id.bt_bookLaterDone:
                Log.e("DF",""+Date_formatter(date,time));
                if(et_dayOfWork.getText().equals("")&&et_timeOfWork.getText().equals("")) {
                    Toast.makeText(this, "make sure date and time are set.", Toast.LENGTH_SHORT).show();
                }
                else {
                    sendbooklater();
                }
                break;
        }
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
         time = ""+hourOfDay+":"+minute+":"+second;
        HourOfDay = hourOfDay;
        Minute = minute;
        Second = second;
        String timeSet = "";
        if (HourOfDay > 12) {
            HourOfDay -= 12;
            timeSet = "PM";
        } else if (HourOfDay == 0) {
            HourOfDay += 12;
            timeSet = "AM";
        } else if (HourOfDay == 12){
            timeSet = "PM";
        }else{
            timeSet = "AM";
        }

        String min = "";
        if (Minute < 10)
            min = "0" + Minute ;
        else
            min = String.valueOf(Minute);

        // Append in a StringBuilder
         time = new StringBuilder().append(HourOfDay).append(':')
                .append(min ).append(" ").append(timeSet).toString();
        et_timeOfWork.setText(time);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
         date = ""+year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
        Year = year;
        MonthOfYear = monthOfYear;
        DayOfMonth = dayOfMonth;
        et_dayOfWork.setText(date);
    }

    public void CustomDialog()
    {
        final android.support.v7.app.AlertDialog.Builder mBuilder = new android.support.v7.app.AlertDialog.Builder(BookLater.this);
        View mView = getLayoutInflater().inflate(R.layout.custom_dialogbox, null);
        mBuilder.setCancelable(false);


        Button btn_accept_amount = (Button) mView.findViewById(R.id.dialogButtonOK);


        mBuilder.setView(mView);

        dialog = mBuilder.create();
        dialog.show();
        btn_accept_amount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i = new Intent(BookLater.this, CategoryActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });
        //aa
    }


    public String Date_formatter(String datee,String time){
        return (""+datee+" "+time);
    }

    public void sendbooklater(){
        AndroidNetworking.post(ConfigURL.URL_POST_BOOK_LATER)
                .addBodyParameter("pMobile", ConfigURL.getMobileNumber(BookLater.this))
                .addBodyParameter("latitude", ConfigURL.getlat(BookLater.this))
                .addBodyParameter("longitude", ConfigURL.getlng(BookLater.this))
                .addBodyParameter("categoryId",ConfigURL.getCategoryid(BookLater.this) )
                .addBodyParameter("jobType","BOOKLATER")
                .addBodyParameter("scheduleTime",""+Date_formatter(date,time) )
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("response",""+response);
                            /*if (!response.getBoolean("error")) {


                            } else if (response.getBoolean("error")) {
                                ProgressDialogClass.dismissDottedProgress();
                                Toast.makeText(BookLater.this, "", Toast.LENGTH_LONG).show();
                            }*/
                            CustomDialog();
                    }

                    @Override
                    public void onError(ANError error) {
                        ProgressDialogClass.dismissDottedProgress();
                        Toast.makeText(BookLater.this, "Some thing went Wrong" + error, Toast.LENGTH_LONG).show();
                    }
                });
    }

    public  void Registerviews() {
        tv_address_booklater = findViewById(R.id.tv_address_booklater);
        et_dayOfWork   = (Button) findViewById(R.id.et_dayOfWork);
        et_timeOfWork = (Button) findViewById(R.id.et_timeOfWork);
        bt_bookLaterDone = (Button) findViewById(R.id.bt_bookLaterDone);
        et_dayOfWork.setOnClickListener(this);
        et_timeOfWork.setOnClickListener(this);
        bt_bookLaterDone.setOnClickListener(this);
    }
}
