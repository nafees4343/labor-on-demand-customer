package com.lod.myapplication_user.Home;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Usama Hussain on 8/20/2016.
 */
public class DateHelper {

    // 2016-05-15
    // 12 August
    // 18
    String strDate;

    public DateHelper(int year, int month , int day)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        Calendar calendar = Calendar.getInstance();
        calendar.set(year , month, day);
        strDate = dateFormat.format(calendar.getTime());
    }
    public DateHelper(String date)
    {
        this.strDate =date;
    }
    /*
    @param strDate ("DATE STRING")
    @return date   ('2016-05-18')
     */
    private Date getFullDate() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = format.parse(this.strDate);
        return date;
    }
    public String getFullDateString() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = format.format(getFullDate());
        return dateString;
    }

    public String getMonthAndDay() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("dd MMM");
        String stringDate = format.format(getFullDate());
        return stringDate;
    }
    public String getMonth() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("MMM");
        String stringDate = format.format(getFullDate());
        return stringDate;
    }
    public String getDay() throws ParseException {
        SimpleDateFormat day = new SimpleDateFormat("d");
        String strDay = day.format(getFullDate());

        return strDay;
    }
    public String getfullDay() throws ParseException {
        SimpleDateFormat day = new SimpleDateFormat("E");
        String strDay = day.format(getFullDate());

        return strDay;
    }

    public int getYearInt() throws ParseException {
        SimpleDateFormat day = new SimpleDateFormat("yyyy");
        String strDay = day.format(getFullDate());
        return Integer.parseInt(strDay);
    }

    public  int getMonthInt() throws ParseException {
        SimpleDateFormat day = new SimpleDateFormat("M");
        String strDay = day.format(getFullDate());
        return Integer.parseInt(strDay);
    }

    public  int getDayInt() throws ParseException {
        SimpleDateFormat day = new SimpleDateFormat("d");
        String strDay = day.format(getFullDate());
        return Integer.parseInt(strDay);
    }
}