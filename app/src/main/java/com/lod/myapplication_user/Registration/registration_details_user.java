package com.lod.myapplication_user.Registration;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.lod.myapplication_user.ProgressDialogClass;
import com.lod.myapplication_user.R;
import com.lod.myapplication_user.UtilsAndUrls.ConfigURL;
import com.lod.myapplication_user.UtilsAndUrls.Wifi_check;

import org.json.JSONException;
import org.json.JSONObject;

public class registration_details_user extends AppCompatActivity implements View.OnClickListener {

    EditText et_user_name_reg,et_email_id_reg,et_phone_no_reg,et_password_reg,et_confirm_password_reg;
    String name,password,confirm_password,email,phone;

    Button bt_next_reg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_details_user);

        //Give context in Fast library
        AndroidNetworking.initialize(getApplicationContext());


        register_click_listener();
        call_click_listener();

    }

    public void register_click_listener()
    {
        et_user_name_reg= (EditText) findViewById(R.id.et_name_reg);
        et_email_id_reg= (EditText) findViewById(R.id.et_email_reg);
        et_phone_no_reg= (EditText) findViewById(R.id.et_phone_no_reg);
        et_password_reg= (EditText) findViewById(R.id.et_password_reg);
        et_confirm_password_reg = (EditText) findViewById(R.id.et_confirm_password_reg);
        bt_next_reg= (Button) findViewById(R.id.bt_next_reg);

    }

    public void call_click_listener()
    {
        bt_next_reg.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.bt_next_reg:
                submitForm();
                break;
        }
    }

    /**
     * Validating form
     */
    private void submitForm() {
        if (!validateFirstName()) {
            return;
        }
        if (!validateEmail()) {
            return;
        }

        if (!validatePhone()) {
            return;
        }
        if (!validatePassword()) {
            return;
        }
        if (!validateConfirmPassword()) {
            return;
        }


        //Sending Values to next form
         name = et_user_name_reg.getText().toString();
         password = et_password_reg.getText().toString();
         confirm_password = et_confirm_password_reg.getText().toString();
         email = et_email_id_reg.getText().toString();
         phone = et_phone_no_reg.getText().toString();
        if(phone.length() == 10){
            phone = "+92"+phone;
        }else {
            phone = "+92"+phone.substring(1);
        }
        //for keyboard hide
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        if(new Wifi_check(getApplication()).isNetworkAvailable()) {
            ProgressDialogClass.showDottedProgress(registration_details_user.this,"Please wait...");
            senddata();
        }
        else{

            Snackbar.make(findViewById(android.R.id.content), "Internet not connected",Snackbar.LENGTH_SHORT).show();

        }
    }

    public void senddata(){
        //***********post user data********
        AndroidNetworking.post(ConfigURL.URL_ISPERSONEXIST)
                .addBodyParameter("pMobile", phone)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressDialogClass.dismissDottedProgress();
                        Toast.makeText(registration_details_user.this, "" + response, Toast.LENGTH_LONG).show();

                        try {
                            if (!response.getBoolean("error")) {
                                //Intent apply data transfer
                                Intent intent = new Intent(registration_details_user.this, registrationAuthActivity.class);
                                intent.putExtra("r_name", name);
                                intent.putExtra("r_email", email);
                                intent.putExtra("r_phone", phone);
                                intent.putExtra("r_password", password);
                                intent.putExtra("r_confirm_password", confirm_password);

                                startActivity(intent);

                            } else if (response.getBoolean("error")) {
                                et_phone_no_reg.setError("Your number already registered");
                            }
                        } catch (JSONException e) {
                            ProgressDialogClass.dismissDottedProgress();
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError error) {
                        ProgressDialogClass.dismissDottedProgress();
                        Toast.makeText(registration_details_user.this, "" + error, Toast.LENGTH_LONG).show();
                    }
                });
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    private boolean validateFirstName() {
        if (et_user_name_reg.getText().toString().trim().isEmpty()) {

            et_user_name_reg.setError(getString(R.string.err_msg_firstName));

            return false;
        }

        return true;
    }

    private boolean validatePassword() {
        if (et_password_reg.getText().toString().trim().isEmpty()) {

            et_password_reg.setError("Please enter password");

            return false;
        }
        return true;
    }

    private boolean validateConfirmPassword() {
        if (et_confirm_password_reg.getText().toString().trim().isEmpty()) {

            et_confirm_password_reg.setError("Please confirm password");
            requestFocus(et_confirm_password_reg);
            return false;
        }
        else  if (!et_password_reg.getText().toString().equals(et_confirm_password_reg.getText().toString())) {
            et_password_reg.setError("Password are not match");
            requestFocus(et_confirm_password_reg);
            return false;
        }

        return true;
    }

    private boolean validateEmail() {
        String email = et_email_id_reg.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            et_email_id_reg.setError(getString(R.string.err_msg_email));
            return false;
        }

        return true;
    }
    private boolean validatePhone() {

        if (et_phone_no_reg.getText().toString().trim().isEmpty()) {
            et_phone_no_reg.setError(getString(R.string.err_msg_phone));

            return false;
        }

        else if(!et_phone_no_reg.getText().toString().matches("[0][3][0-9]{9}|[3][0-9]{9}"))
        {
            et_phone_no_reg.setError(getString(R.string.err_regex_msg_phone));

            return false;
        }
        else  if(et_phone_no_reg.getText().toString().length() < 10  ){
            et_phone_no_reg.setError(getString(R.string.err_regex_msg_phone));

            return false;
        }

        return true;
    }


}
