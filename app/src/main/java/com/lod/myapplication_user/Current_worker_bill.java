package com.lod.myapplication_user;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.lod.myapplication_user.Category.CategoryActivity;
import com.lod.myapplication_user.UtilsAndUrls.ConfigURL;
import com.lod.myapplication_user.UtilsAndUrls.Jobdetails_SharedPreference;
import com.lod.myapplication_user.UtilsAndUrls.Wifi_check;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class Current_worker_bill extends AppCompatActivity implements View.OnClickListener {

    Button bt_submit_current_worker_bill;
    ImageView iv_current_worker_bill;
    TextView tv_name_worker_bill, tv_title_worker_bill, tv_cost_worker_bill, tv_pay_worker_bill;
    RatingBar rb_worker_bill;
    String jobId,jobCost, worker_name, worker_title, worker_image;
    String customerPay;
    float getRatingbyCustomer;
    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_worker_bill);
        //get Data from bundle
        //GetBundle();
        //Register All Views
        RegisterViews();
        //Set All texts and params in views
        SetTexts_Of_All_Views();
        //Call Click listeners
        click_listeners();

    }


    public void click_listeners() {

        bt_submit_current_worker_bill.setOnClickListener(this);

    }

    public void RegisterViews() {

        tv_name_worker_bill = (TextView) findViewById(R.id.tv_name_current_worker_bill);
        tv_cost_worker_bill = (TextView) findViewById(R.id.tv_cost_current_worker_bill);
        tv_pay_worker_bill = (TextView) findViewById(R.id.tv_customerPay_current_worker_pay);
        tv_title_worker_bill = (TextView) findViewById(R.id.tv_title_current_worker_bill);
        iv_current_worker_bill = (ImageView) findViewById(R.id.iv_current_worker_bill_image);
        bt_submit_current_worker_bill = (Button) findViewById(R.id.bt_submit_current_worker_bill);
        rb_worker_bill = (RatingBar) findViewById(R.id.rb_worker_bill);


    }


    public void SetTexts_Of_All_Views() {

        tv_name_worker_bill.setText(Jobdetails_SharedPreference.getworkerName(Current_worker_bill.this));
        tv_title_worker_bill.setText(Jobdetails_SharedPreference.getjobTitle(Current_worker_bill.this));
        tv_cost_worker_bill.setText(Jobdetails_SharedPreference.getjobCost(Current_worker_bill.this));
        tv_pay_worker_bill.setText(Jobdetails_SharedPreference.getcustomerPay(Current_worker_bill.this));
        if(Jobdetails_SharedPreference.getworkerImage(Current_worker_bill.this) != null) {

                Picasso.with(Current_worker_bill.this)
                        .load(Jobdetails_SharedPreference.getworkerImage(Current_worker_bill.this))
                        .into(iv_current_worker_bill);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.bt_submit_current_worker_bill:
                if(rb_worker_bill.getRating()==0.0){
                    Toast.makeText(Current_worker_bill.this,"Please rate your labour",Toast.LENGTH_SHORT).show();
                }
                else {

                    if (new Wifi_check(this).isNetworkAvailable()) {
                        //***********url for login********
                        ProgressDialogClass.showDottedProgress(Current_worker_bill.this,"Please wait...");
                        getRatingbyCustomer = rb_worker_bill.getRating();
 //                       Toast.makeText(Current_worker_bill.this,"rating : "+getRatingbyCustomer,Toast.LENGTH_SHORT).show();
                        Send_rating();
                    } else {

                        Snackbar.make(findViewById(android.R.id.content), "Internet not connected", Snackbar.LENGTH_SHORT).show();

                    }

                }
                break;
        }
    }



    public void Send_rating(){

        AndroidNetworking.post(ConfigURL.URL_SEND_RATING_BY_CUSTOMER)
                .addBodyParameter("jobId",Jobdetails_SharedPreference.getjobId(Current_worker_bill.this))
                .addBodyParameter("rateByCustomer",""+getRatingbyCustomer)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressDialogClass.dismissDottedProgress();
                        try {
                            if (!response.getBoolean("error")) {
                                //Intent apply data transfer
                                i = new Intent(Current_worker_bill.this, CategoryActivity.class);
                                startActivity(i);
                                finish();

                            } else if (response.getBoolean("error")) {
                                ProgressDialogClass.dismissDottedProgress();
                                Toast.makeText(Current_worker_bill.this, "Some thing went wrong", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        ProgressDialogClass.dismissDottedProgress();
                        //Toast.makeText(Current_worker_bill.this, "eRr" + error, Toast.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    public void onBackPressed() {

    }


}
