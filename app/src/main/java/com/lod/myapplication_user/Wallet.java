package com.lod.myapplication_user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.lod.myapplication_user.Category.CategoryActivity;

public class Wallet extends AppCompatActivity implements View.OnClickListener{

    Button bt_done_wallet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);

      /*  Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_wallet);
        toolbar.setTitle("Wallet");
        setSupportActionBar(toolbar);
*/

        bt_done_wallet = (Button) findViewById(R.id.bt_done_wallet);

        bt_done_wallet.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch(view.getId())
        {
            case R.id.bt_done_wallet:
                Intent i = new Intent(this, CategoryActivity.class);
                startActivity(i);
                break;
        }
    }
}
