package com.lod.myapplication_user.Forget_Pass;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.lod.myapplication_user.R;
import com.lod.myapplication_user.UtilsAndUrls.ConfigURL;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by nfs on 8/31/2017.
 */


public class activity_phone_no_forgot_pass extends Activity implements View.OnClickListener{

    String  r_phone;
    private EditText input_phone_no_forget_pass_user;
    private TextInputLayout inputLayoutPassword, inputLayoutPasswordConfirmation;
    private Button bt_proceed_forgot_pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_no_forget_pass);

        input_phone_no_forget_pass_user = (EditText) findViewById(R.id.input_phone_no_forget_pass_user);
        bt_proceed_forgot_pass = (Button) findViewById(R.id.bt_proceed_forgot_pass);


        bt_proceed_forgot_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });
    }

    private void submitForm() {
        if (!validatePhone()) {
            return;
        }

        showConfirmationDialog(add92(input_phone_no_forget_pass_user.getText().toString()));

    }


    private boolean validatePhone() {
        if (!input_phone_no_forget_pass_user.getText().toString().matches("[0][3][0-9]{9}|[3][0-9]{9}")) {
            input_phone_no_forget_pass_user.setError(getString(R.string.err_regex_msg_phone));

            return false;
        } else if (input_phone_no_forget_pass_user.getText().toString().length() < 10) {
            input_phone_no_forget_pass_user.setError(getString(R.string.err_regex_msg_phone));

            return false;
        } else if (input_phone_no_forget_pass_user.getText().toString().trim().isEmpty()) {
            input_phone_no_forget_pass_user.setError(getString(R.string.err_msg_phone));

            return false;
        }

        return true;
    }

    private void showConfirmationDialog(String phoneNumber) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity_phone_no_forgot_pass.this);
        builder.setTitle(getString(R.string.dialog_title_phoneNumber_confirmation));
        builder.setMessage(phoneNumber);
        r_phone = phoneNumber;
        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        if(isNetworkAvailable()) {
                            //***********post user data********
                            AndroidNetworking.post(ConfigURL.URL_ISPERSONEXIST)
                                    .addBodyParameter("pMobile", r_phone)
                                    .setTag("test")
                                    .setPriority(Priority.MEDIUM)
                                    .build()
                                    .getAsJSONObject(new JSONObjectRequestListener() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            try {
                                                if (response.getBoolean("error")) {
                                                    //Intent apply data transfer
                                                    Intent intent = new Intent(activity_phone_no_forgot_pass.this, forgotpassAuthActivity.class);
                                                    intent.putExtra("r_phone",r_phone);
                                                    startActivity(intent);

                                                } else if (!response.getBoolean("error")) {
                                                    input_phone_no_forget_pass_user.setError("You are not registered");
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }


                                        }

                                        @Override
                                        public void onError(ANError error) {
                                            Toast.makeText(activity_phone_no_forgot_pass.this, "" + error, Toast.LENGTH_LONG).show();
                                        }
                                    });
                        }
                        else{

                            Snackbar.make(findViewById(android.R.id.content), "Internet not connected",Snackbar.LENGTH_SHORT).show();

                        }


                    }
                });

        String negativeText = getString(R.string.edit);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //editPhoneNumber();
                        dialog.dismiss();
                        }
                });

        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    private void editPhoneNumber() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        final EditText input = new EditText(this);
        input.setSingleLine();
        FrameLayout container = new FrameLayout(this);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.leftMargin = getResources().getDimensionPixelSize(R.dimen.dialog_margin);
        params.rightMargin = getResources().getDimensionPixelSize(R.dimen.dialog_margin);
        params.topMargin = getResources().getDimensionPixelSize(R.dimen.dialog_margin);
        input.setLayoutParams(params);
        input.setText(input_phone_no_forget_pass_user.getText().toString());
        container.addView(input);
        builder.setTitle("Edit Phone Number");
        /*builder.setMessage("Phone number");*/
        builder.setView(container);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {


                r_phone = add92(input.getText().toString());


                Intent intent = new Intent(activity_phone_no_forgot_pass.this, forgotpassAuthActivity.class);
                //Sending Values to next form
                intent.putExtra("r_phone", r_phone);
                startActivity(intent);
            }
        });
        /*builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });*/

        builder.show();
    }

    public String add92(String phone_no) {
        if (phone_no.length() == 10) {
            phone_no = "+92" + phone_no;
        } else {
            phone_no = "+92" + phone_no.substring(1);
        }
        return phone_no;
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btn_next:
                break;
        }
    }
}
