package com.lod.myapplication_user.Forget_Pass;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.lod.myapplication_user.Category.CategoryActivity;
import com.lod.myapplication_user.R;
import com.lod.myapplication_user.UtilsAndUrls.ConfigURL;
import com.lod.myapplication_user.UtilsAndUrls.Wifi_check;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by mmustafa on 8/31/2017.
 */


public class forgotPasswordConfirmationActivity extends Activity {

    String r_fname, r_lname, r_uname, r_phone, r_email;
    Context context;
    private EditText inputPassword, inputPasswordConfirmation;
    private TextInputLayout inputLayoutPassword, inputLayoutPasswordConfirmation;
    private Button btnNext;
    Boolean error;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_pass_confirmation);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            r_phone = (String) bundle.get("r_phone");

        }

        inputPassword = (EditText) findViewById(R.id.et_password_forgotpass);

        inputPasswordConfirmation = (EditText) findViewById(R.id.et_passwordConfirmation_forgotpass);

        btnNext = (Button) findViewById(R.id.btn_next);




        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });
    }

    private void submitForm() {
        if (!validatePassword()) {
            return;
        }
        if (!validatePasswordConfirmation()) {
            return;
        }
        sendData();
    }


    private boolean validatePassword() {
        if (inputPassword.getText().toString().trim().isEmpty()) {
            inputPassword.setError(getString(R.string.err_msg_password));
            requestFocus(inputPassword);
            return false;
        }
        return true;
    }


    private boolean validatePasswordConfirmation() {
        if (inputPasswordConfirmation.getText().toString().trim().isEmpty()) {
            inputPasswordConfirmation.setError(getString(R.string.err_msg_password_confirmation));
            requestFocus(inputPasswordConfirmation);
            return false;
        } else if (!inputPassword.getText().toString().equals(inputPasswordConfirmation.getText().toString())) {
            inputPassword.setError(getString(R.string.err_msg_password_confirmation_not_match));
            requestFocus(inputPasswordConfirmation);
            return false;
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    public void sendData() {

        if (new Wifi_check(getApplication()).isNetworkAvailable()) {
            //***********post user data********
            AndroidNetworking.post(ConfigURL.URL_FORGOTPASSWORD)
                    .addBodyParameter("pMobile", r_phone)
                    .addBodyParameter("pPass", inputPassword.getText().toString())

                    .setTag("test")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response.getBoolean("error")) {
                                    //Intent apply
                                    Intent intent = new Intent(forgotPasswordConfirmationActivity.this, CategoryActivity.class);
                                    startActivity(intent);

                                } else if (!response.getBoolean("error")) {
                                   // input_phone_no_forget_pass_user.setError("Your number already registered");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError error) {
                            Toast.makeText(forgotPasswordConfirmationActivity.this, "" + error, Toast.LENGTH_LONG).show();
                        }
                    });
        } else {

            Snackbar.make(findViewById(android.R.id.content), "Internet not connected", Snackbar.LENGTH_SHORT).show();

        }

    }



}
