package com.lod.myapplication_user.Models;

/**
 * Created by Admin on 12/27/2017.
 */

public class Worker  {
    public String personId;
    public String personName;
    public String workerImage;
    public String latitude;
    public String longitude;
    public String personMobileNo;

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getWorkerImage() {
        return workerImage;
    }

    public void setWorkerImage(String workerImage) {
        this.workerImage = workerImage;
    }

    public String getLatitude() {
        return latitude;
    }

    public Worker(String personId, String personName, String workerImage, String latitude, String longitude, String personMobileNo) {
        this.personId = personId;
        this.personName = personName;
        this.workerImage = workerImage;
        this.latitude = latitude;
        this.longitude = longitude;
        this.personMobileNo = personMobileNo;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPersonMobileNo() {
        return personMobileNo;
    }

    public void setPersonMobileNo(String personMobileNo) {
        this.personMobileNo = personMobileNo;
    }


   /* protected Worker(Parcel in) {

        this.personName = in.readString();
        this.workerImage = in.readString();
        this.latitude = in.readString();
        this.longitude= in.readString();
        this.personMobileNo = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(personName);
        parcel.writeString(workerImage);
        parcel.writeString(latitude);
        parcel.writeString(longitude);
        parcel.writeString(personMobileNo);
    }

    public static final Creator<Worker> CREATOR = new Creator<Worker>() {
        @Override
        public Worker createFromParcel(Parcel in) {
            return new Worker(in);
        }

        @Override
        public Worker[] newArray(int size) {
            return new Worker[size];
        }
    };*/
 }
