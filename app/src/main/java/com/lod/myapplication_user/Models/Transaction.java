package com.lod.myapplication_user.Models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Admin on 1/4/2018.
 */

public class Transaction {
    public String jobId;
    public String Transfer;
    public String Amount;
    public String updateTime;
    public String Balance;

    public Transaction(String jobId, String transfer, String amount, String updateTime, String balance) {
        this.jobId = jobId;
        Transfer = transfer;
        Amount = amount;
        this.updateTime = updateTime;
        Balance = balance;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getTransfer() {
        return Transfer;
    }

    public void setTransfer(String transfer) {
        Transfer = transfer;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getBalance() {
        return Balance;
    }

    public void setBalance(String balance) {
        Balance = balance;
    }

    public Transaction(JSONObject jsonObject) {
        try {
            this.jobId = jsonObject.getString("jobId");
            this.Transfer = jsonObject.getString("Transfer");
            this.Amount = jsonObject.getString("Amount");
            this.updateTime = jsonObject.getString("updateTime");
            this.Balance = jsonObject.getString("Balance");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
