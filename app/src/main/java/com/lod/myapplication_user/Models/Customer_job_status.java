package com.lod.myapplication_user.Models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Admin on 2/10/2018.
 */

public class Customer_job_status {

    String latitude;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    String longitude;
    String type;
    String jobId;
    String state;
    String workerName;
    String workerImage;
    String jobStatus;
    String jobTitle;
    String workerPhoneNo;
    String work_status;
    String operator;
    String jobCost;
    String customerPay;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getWorkerName() {
        return workerName;
    }

    public void setWorkerName(String workerName) {
        this.workerName = workerName;
    }

    public String getWorkerImage() {
        return workerImage;
    }

    public void setWorkerImage(String workerImage) {
        this.workerImage = workerImage;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getWorkerPhoneNo() {
        return workerPhoneNo;
    }

    public void setWorkerPhoneNo(String workerPhoneNo) {
        this.workerPhoneNo = workerPhoneNo;
    }

    public String getWork_status() {
        return work_status;
    }

    public void setWork_status(String work_status) {
        this.work_status = work_status;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Customer_job_status() {

    }

    public Customer_job_status(String type, String jobId, String workerName, String workerImage, String jobStatus, String jobTitle, String workerPhoneNo, String work_status, String operator, String jobCost, String customerPay, String latitude, String longitude) {
        this.type = type;
        this.jobId = jobId;
        this.workerName = workerName;
        this.workerImage = workerImage;
        this.jobStatus = jobStatus;
        this.jobTitle = jobTitle;
        this.workerPhoneNo = workerPhoneNo;
        this.work_status = work_status;
        this.operator = operator;
        this.jobCost = jobCost;
        this.customerPay = customerPay;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getJobCost() {

        return jobCost;
    }

    public void setJobCost(String jobCost) {
        this.jobCost = jobCost;
    }

    public String getCustomerPay() {
        return customerPay;
    }

    public void setCustomerPay(String customerPay) {
        this.customerPay = customerPay;
    }

    public Customer_job_status(JSONObject jsonObject) {
        try {

            this.jobId = jsonObject.getString("jobId");
            this.jobStatus = jsonObject.getString("jobStatus");
            this.latitude = jsonObject.getString("latitude");
            this.longitude = jsonObject.getString("longitude");
            this.jobTitle = jsonObject.getString("jobTitle");
            this.jobCost = jsonObject.getString("jobCost");
            this.customerPay = jsonObject.getString("recievedCash");
            this.workerName = jsonObject.getString("workerName");
            this.workerPhoneNo = jsonObject.getString("workerMobileNo");
            this.workerImage = jsonObject.getString("workerImage");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
