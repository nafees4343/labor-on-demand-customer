package com.lod.myapplication_user.Models;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by nafees on 12/8/2017.
 */

public class Customer_completed_orders {

    public String jobcanceledBy, RattingByWorker, RattingByCustomer, jobId, jobDate, jobTime, jobStatus, jobAddress, jobBookingStatus, jobCost, jobHours, jobStartTime, jobCompleteTime, jobTitle, personName;


    public String getRattingByWorker() {
        return RattingByWorker;
    }

    public void setRattingByWorker(String rattingByWorker) {
        RattingByWorker = rattingByWorker;
    }

    public String getRattingByCustomer() {
        return RattingByCustomer;
    }

    public void setRattingByCustomer(String rattingByCustomer) {
        RattingByCustomer = rattingByCustomer;
    }


    public Customer_completed_orders(String personName , String jobStatus, String jobAddress, String jobBookingStatus, String jobCost, String jobHours, String jobStartTime, String jobCompleteTime, String jobTitle, String RattingByWorker, String RattingByCustomer) {
        this.personName = personName;
        this.jobStatus = jobStatus;
        this.jobAddress = jobAddress;
        this.jobBookingStatus = jobBookingStatus;
        this.jobCost = jobCost;
        this.jobHours = jobHours;
        this.jobStartTime = jobStartTime;
        this.jobCompleteTime = jobCompleteTime;
        this.jobTitle = jobTitle;
        this.RattingByCustomer = RattingByCustomer;
        this.RattingByWorker = RattingByWorker;

    }


    public String getPersonName() {
        return personName;

    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getJobDate() {
        return jobDate;
    }



    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public String getJobAddress() {
        return jobAddress;
    }

    public void setJobAddress(String jobAddress) {
        this.jobAddress = jobAddress;
    }

    public String getJobBookingStatus() {
        return jobBookingStatus;
    }

    public void setJobBookingStatus(String jobBookingStatus) {
        this.jobBookingStatus = jobBookingStatus;
    }

    public String getJobCost() {
        return jobCost;
    }

    public void setJobCost(String jobCost) {
        this.jobCost = jobCost;
    }

    public String getJobHours() {
        return jobHours;
    }

    public void setJobHours(String jobHours) {
        this.jobHours = jobHours;
    }

    public String getJobStartTime() {
        return jobStartTime;
    }

    public void setJobStartTime(String jobStartTime) {
        this.jobStartTime = jobStartTime;
    }

    public String getJobCompleteTime() {
        return jobCompleteTime;
    }

    public void setJobCompleteTime(String jobCompleteTime) {
        this.jobCompleteTime = jobCompleteTime;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }


    public Customer_completed_orders(JSONObject jsonObject) {
        try {
            this.jobId = jsonObject.getString("jobId");
            this.personName = jsonObject.getString("personName");
            this.jobStatus = jsonObject.getString("jobStatus");
            this.jobBookingStatus = jsonObject.getString("jobBookingStatus");
            this.jobCost = jsonObject.getString("jobCost");
            this.jobHours = jsonObject.getString("jobHours");
            this.jobStartTime = jsonObject.getString("jobCreationTime");
            this.jobCompleteTime = jsonObject.getString("jobCompleteTime");
            this.jobTitle = jsonObject.getString("jobTitle");
            this.RattingByCustomer = jsonObject.getString("RattingByCustomer");
            this.RattingByWorker = jsonObject.getString("RattingByWorker");
            }
        catch (JSONException e) {
            e.printStackTrace();
            Log.v("COMPLETEDORDERS"," error "+ e.getLocalizedMessage());
        }
    }

}





