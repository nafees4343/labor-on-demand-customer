package com.lod.myapplication_user.Models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by nafees on 11/18/2017.
 */

public class Customer_profile {

     int customer_id;
     String customer_name;
     String customer_email;
     String customer_phone_no;
     String customer_password;

    public Customer_profile(String customer_name, String customer_email, String customer_phone_no) {
        this.customer_id = customer_id;
        this.customer_name = customer_name;
        this.customer_email = customer_email;
        this.customer_phone_no = customer_phone_no;
        this.customer_password = customer_password;
    }

    public Customer_profile(){}


    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_email() {
        return customer_email;
    }

    public void setCustomer_email(String customer_email) {
        this.customer_email = customer_email;
    }

    public String getCustomer_phone_no() {
        return customer_phone_no;
    }

    public void setCustomer_phone_no(String customer_phone_no) {
        this.customer_phone_no = customer_phone_no;
    }

    public String getCustomer_password() {
        return customer_password;
    }

    public void setCustomer_password(String customer_password) {
        this.customer_password = customer_password;
    }



    public Customer_profile(JSONObject jsonObject)
    {
        try {
            this.customer_name = jsonObject.getString("personName");
            this.customer_email = jsonObject.getString("personEmail");
            this.customer_phone_no =jsonObject.getString("personMobileNo");
            this.customer_password = jsonObject.getString("personPassword");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
