package com.lod.myapplication_user.Models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Admin on 12/18/2017.
 */

public class Category implements Parcelable{

    public String categoryname,categoryStatus,categoryImage,categoryId;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public String getCategoryStatus() {
        return categoryStatus;
    }

    public void setCategoryStatus(String categoryStatus) {
        this.categoryStatus = categoryStatus;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public Category(String categoryId,String categoryname, String categoryStatus, String categoryImage) {
        this.categoryname = categoryname;
        this.categoryStatus = categoryStatus;
        this.categoryImage = categoryImage;
        this.categoryId = categoryId;
    }

    public Category(JSONObject jsonObject)
    {
        try {
            this.categoryId = jsonObject.getString("categoryId");
            this.categoryname =jsonObject.getString("categoryname");
            this.categoryStatus = jsonObject.getString("categoryStatus");
            this.categoryImage = jsonObject.getString("categoryImage");


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    protected Category(Parcel in) {
        this.categoryId = in.readString();
        this.categoryname = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(categoryId);
        parcel.writeString(categoryname);
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
}
