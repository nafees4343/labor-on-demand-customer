package com.lod.myapplication_user.Login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.lod.myapplication_user.Category.CategoryActivity;
import com.lod.myapplication_user.Current_worker;
import com.lod.myapplication_user.Forget_Pass.activity_phone_no_forgot_pass;
import com.lod.myapplication_user.Models.Customer_job_status;
import com.lod.myapplication_user.Models.Customer_profile;
import com.lod.myapplication_user.ProgressDialogClass;
import com.lod.myapplication_user.R;
import com.lod.myapplication_user.Registration.registration_details_user;
import com.lod.myapplication_user.UtilsAndUrls.ConfigURL;
import com.lod.myapplication_user.UtilsAndUrls.Jobdetails_SharedPreference;
import com.lod.myapplication_user.UtilsAndUrls.KeyboardUtil;
import com.lod.myapplication_user.UtilsAndUrls.Wifi_check;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tv_forget_pass, tv_signup_user;
    Button bt_login_user;
    EditText et_phone_no_login, et_password_login;
    Intent i;
    String LOGIN="active";
    String password;
    String phone_no;
    String pName;
    String pEmail;
    ArrayList<Customer_profile> arrayList;
    String customer_name, customer_phone_no, customer_email;
    ProgressDialog progressDialog;
    ArrayList<Customer_job_status> data;
    String latitude, longitude, type, jobId, state, workerName, workerImage, jobStatus, jobTitle, workerPhoneNo, work_status, operator, jobCost, customerPay;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);
        register_views();
        call_clicklistener();
        //for keyboard hide
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }



    public void senddata()
    {
         password = et_password_login.getText().toString();
         phone_no = et_phone_no_login.getText().toString();

        if (phone_no.length() == 10) {
            phone_no = "+92" + phone_no;
        } else {
            phone_no = "+92" + phone_no.substring(1);
        }

        KeyboardUtil.hideSoftKeyboard(LoginActivity.this);

        if (new Wifi_check(this).isNetworkAvailable()) {
            //***********url for login********
            ProgressDialogClass.showDottedProgress(LoginActivity.this,"Please wait...");
            get_login_detail();

        } else {

            Snackbar.make(findViewById(android.R.id.content), "Internet not connected", Snackbar.LENGTH_SHORT).show();

        }
    }

    public void get_login_detail(){

        AndroidNetworking.post(ConfigURL.URL_LOGINCUSTOMER)
                .addBodyParameter("pMobile", phone_no)
                .addBodyParameter("pPass", password)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            if (!response.getBoolean("error")) {

                                JSONArray jsonArray = response.getJSONArray("customer");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    Customer_profile category = new Customer_profile(jsonArray.getJSONObject(i));
                                    customer_name = category.getCustomer_name();
                                    customer_phone_no = category.getCustomer_phone_no();
                                    customer_email = category.getCustomer_email();
                                }

                                SharedPreferences preferences = getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("LOGIN", LOGIN);
                                editor.putString("PHONE", phone_no);
                                editor.putString("NAME", customer_name);
                                editor.putString("EMAIL", customer_email);
                                editor.commit();
                               // Toast.makeText(LoginActivity.this," Phone "+ customer_phone_no+" Name "+customer_name+" Email "+customer_email, Toast.LENGTH_LONG).show();


                                checkJobStatus();


                                sendRegistrationToServer();
                                ProgressDialogClass.dismissDottedProgress();
                                finish();

                            } else if (response.getBoolean("error")) {
                                ProgressDialogClass.dismissDottedProgress();
                                Toast.makeText(LoginActivity.this, "Wrong phone/password", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        ProgressDialogClass.dismissDottedProgress();
                        Toast.makeText(LoginActivity.this, "" + error, Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void submitForm() {

        if (!validatePhone()) {
            return;
        }
        if (!validatePassword()) {
            return;
        }

        senddata();


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.tv_trouble_user:
                Intent i = new Intent(LoginActivity.this, activity_phone_no_forgot_pass.class);
                startActivity(i);
                break;
            case R.id.bt_submit_login:
                submitForm();
                break;
            case R.id.bt_signup_user:
                i = new Intent(LoginActivity.this, registration_details_user.class);
                startActivity(i);
                break;

        }
    }


    public void register_views() {

        tv_forget_pass = (TextView) findViewById(R.id.tv_trouble_user);
        bt_login_user = (Button) findViewById(R.id.bt_submit_login);
        tv_signup_user = (TextView) findViewById(R.id.bt_signup_user);

        et_phone_no_login = (EditText) findViewById(R.id.et_phone_no_login);
        et_password_login = (EditText) findViewById(R.id.et_password_login);

    }

    public void call_clicklistener() {

        tv_forget_pass.setOnClickListener(this);
        bt_login_user.setOnClickListener(this);
        tv_signup_user.setOnClickListener(this);
    }


    private boolean validatePhone() {

        if (et_phone_no_login.getText().toString().trim().isEmpty()) {
            et_phone_no_login.setError(getString(R.string.err_msg_phone));

            return false;
        } else if (!et_phone_no_login.getText().toString().matches("[0][3][0-9]{9}|[3][0-9]{9}")) {
            et_phone_no_login.setError(getString(R.string.err_regex_msg_phone));

            return false;
        } else if (et_phone_no_login.getText().toString().length() < 10) {
            et_phone_no_login.setError(getString(R.string.err_regex_msg_phone));

            return false;
        }

        return true;
    }



    private boolean validatePassword() {
        if (et_password_login.getText().toString().trim().isEmpty()) {

            et_password_login.setError("Please enter password");

            return false;
        }
        return true;
    }

    private void sendRegistrationToServer() {
        String URL = "http://www.itehadmotors.com/qasimWork/laborondemandwebservice/v1/insertfcmcustomer";
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        AndroidNetworking.initialize(getApplicationContext());
        AndroidNetworking.post(URL)
                .addBodyParameter("apiKey", refreshedToken)
                .addBodyParameter("customerMobileNo", ConfigURL.getMobileNumber(this))
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.v("FIREBASE:result",""+response);
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.v("FIREBASE:error",""+error);
                    }
                });
    }



    public void checkJobStatus() {

        AndroidNetworking.get(ConfigURL.URL_CUSTOMER_JOB_STATUS)
                .addQueryParameter("customerMobileNo", ConfigURL.getMobileNumber(this))
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        Log.d("LResponse", "Response: " + response);
                        //ProgressDialogClass.dismissDottedProgress();


                        try {
                            /*Boolean error = false;
                            error = response.getBoolean("error");*/
                            Log.d("AAA", " TRY");

                            JSONArray jsonArray = response.getJSONArray("customerJob");

                            if (jsonArray.length()<=0) {

                                Log.d("BBB", "empty");
                                Intent intent = new Intent(LoginActivity.this, CategoryActivity.class);
                                startActivity(intent);
                                finish();

                            } else {
                                Log.d("BBB", "not empty");

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    Customer_job_status category = new Customer_job_status(jsonArray.getJSONObject(i));
                                    jobId = category.getJobId();
                                    jobStatus = category.getJobStatus();
                                    workerName = category.getWorkerName();
                                    workerPhoneNo = category.getWorkerPhoneNo();
                                    jobCost = category.getJobCost();
                                    workerImage = category.getWorkerImage();
                                    jobStatus = category.getJobStatus();
                                    jobTitle = category.getJobTitle();
                                    work_status = category.getWork_status();
                                    operator = category.getOperator();
                                    customerPay = category.getCustomerPay();
                                    latitude = category.getLatitude();
                                    longitude = category.getLongitude();

                                }

                                SetJobStatus();
                                Log.d("JB_status", "\ntype: " + type + "\njobStatus: " + jobStatus + "\njob iD" + jobId + "\nwork name: " + workerName + "\nworker Phone no" + workerPhoneNo + "\ncost: " + jobCost);

                                Jobdetails_SharedPreference.get_work_details_in_shared_preference(getApplicationContext(), latitude, longitude, workerName, workerImage, workerPhoneNo, jobStatus, jobTitle, jobId, jobCost, customerPay, jobStatus);

                                Intent intent = new Intent(LoginActivity.this, Current_worker.class);
                                startActivity(intent);
                                finish();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        //ProgressDialogClass.dismissDottedProgress();
                        Toast.makeText(LoginActivity.this, "" + anError, Toast.LENGTH_LONG).show();

                    }
                });
    }

    public void SetJobStatus() {

        Log.d("jb",""+jobStatus);

        if(jobStatus!=null) {

            switch (jobStatus + "") {

                case "ACTIVE":
                    jobStatus = "Job Recived";
                    break;
                case "ARRIVED":
                    jobStatus = "Job Arrived";
                    break;
                case "START":
                    jobStatus = "Job Start";
                    break;
                case "COMPLETED":
                    jobStatus = "Job Complete";
                    break;
            }
        }
    }




}
