package com.lod.myapplication_user;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ParseException;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.lod.myapplication_user.Category.CategoryActivity;
import com.lod.myapplication_user.Services.MyService;
import com.lod.myapplication_user.UtilsAndUrls.ConfigURL;
import com.lod.myapplication_user.UtilsAndUrls.Jobdetails_SharedPreference;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class Current_worker extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    TextView tv_job_cancel_job, tv_current_worker_name, tv_jobTitle_current, tv_work_status, tv_worker_phoneNo;
    private GoogleMap mMap;
    private Marker mMarker;
    LatLng latLng;
    ImageView iv_workerImage;
    Double latitude = 0.0, longitude = 0.0;
    Float workerRattings = 0.0f;
    boolean isError = false;
    //RatingBar rb_current_worker;
    String type, jobId, state, workerName, workerImage, jobStatus, jobTitle, workerPhoneNo, work_status, operator, jobCost, customerPay;
    MyReceiver myReceiver;
    Intent i;
    AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_worker);

        RegisterViews();

        tv_job_cancel_job.setOnClickListener(this);


        //for map fragment
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_current_work);
        mapFragment.getMapAsync(this);

        onNewIntent(getIntent());

    }

    public void SetTexts_Of_All_Views() {

        if (Jobdetails_SharedPreference.getworkerImage(Current_worker.this) != null) {
            if (!Jobdetails_SharedPreference.getworkerImage(Current_worker.this).equals("")) {
                Picasso.with(Current_worker.this)
                        .load(workerImage)
                        .into(iv_workerImage);
            }
        } else {
            iv_workerImage.setImageResource(R.drawable.ic_default_pic);
        }
        tv_current_worker_name.setText(Jobdetails_SharedPreference.getworkerName(Current_worker.this));
        tv_jobTitle_current.setText(Jobdetails_SharedPreference.getjobTitle(Current_worker.this));
        tv_worker_phoneNo.setText(Jobdetails_SharedPreference.getworkerPhoneNo(Current_worker.this));

        switch (Jobdetails_SharedPreference.gettype(Current_worker.this)) {
            case "Job Recived":
                tv_work_status.setText("Your worker has confirmed");
                break;
            case "Job Arrived":
                tv_work_status.setText("Your worker has been arrived");
                break;
            case "Job Start":
                tv_work_status.setText("Your work has been started");
                break;
            case "Job Complete":
                tv_work_status.setText("Your task is completed");
                break;


        }

    }

    public void get_params() {
        try {
            if (!Jobdetails_SharedPreference.getlatitude(Current_worker.this).equals("") || !Jobdetails_SharedPreference.getlongitude(Current_worker.this).equals("")) {

                if (!Jobdetails_SharedPreference.getlatitude(Current_worker.this).equals("null") || !Jobdetails_SharedPreference.getlongitude(Current_worker.this).equals("null")) {
                    latitude = Double.parseDouble(Jobdetails_SharedPreference.getlatitude(Current_worker.this));
                    longitude = Double.parseDouble(Jobdetails_SharedPreference.getlongitude(Current_worker.this));
                } else {
                    latitude = 0.0;
                    longitude = 0.0;
                }

            } else {
                latitude = 0.0;
                longitude = 0.0;
            }
        } catch (ParseException e) {
            Log.e("CurrentWorker", "" + e.getLocalizedMessage());
        }
        workerName = Jobdetails_SharedPreference.getworkerName(Current_worker.this);
        workerImage = Jobdetails_SharedPreference.getworkerImage(Current_worker.this);
        workerPhoneNo = Jobdetails_SharedPreference.getworkerPhoneNo(Current_worker.this);
        jobStatus = Jobdetails_SharedPreference.getjobStatus(Current_worker.this);
        jobTitle = Jobdetails_SharedPreference.getjobTitle(Current_worker.this);
        work_status = Jobdetails_SharedPreference.getjobStatus(Current_worker.this);
        jobId = Jobdetails_SharedPreference.getjobId(Current_worker.this);
        type = Jobdetails_SharedPreference.gettype(Current_worker.this);
    }


    public void updateStatus(String Jobstatus) {
        tv_work_status.setText(Jobstatus);
    }

    public void RegisterViews() {

        tv_job_cancel_job = (TextView) findViewById(R.id.tv_cancel_job);
        iv_workerImage = (ImageView) findViewById(R.id.iv_workerImage);
        tv_current_worker_name = (TextView) findViewById(R.id.tv_worker_name_current);
        tv_work_status = (TextView) findViewById(R.id.tv_work_status);
        tv_jobTitle_current = (TextView) findViewById(R.id.tv_jobTitle_current);
        tv_worker_phoneNo = (TextView) findViewById(R.id.tv_worker_phoneNo);

    }

    @Override
    public void onBackPressed() {
    }

    public void SelectState() {

        get_params();
        operator = Jobdetails_SharedPreference.gettype(Current_worker.this);
        switch (operator) {
            case "Job Recived":
                //Set All Texts
                SetTexts_Of_All_Views();
                RegisterReceiverAndStartService();

                // get_work_details_in_shared_preference(latitude,longitude,state,workerName,workerImage,workerPhoneNo,jobStatus,jobTitle,work_status,jobId,jobCost,customerPay,type);
                Log.d("JobDetails", "latitude: " + Jobdetails_SharedPreference.getlatitude(Current_worker.this) + "\nlongitude: " + Jobdetails_SharedPreference.getlongitude(Current_worker.this) + "\njoBstatus: " + Jobdetails_SharedPreference.gettype(Current_worker.this));
                Log.d("TTT", "Received");
                break;
            case "Job Arrived":
                UnregisterReceiverAndStopService();
                SetTexts_Of_All_Views();
                Log.d("TTT", "Arrived");
                if (ConfigURL.get_confirm_amount_param(Current_worker.this).equals("true")) {
                    jobcost_confirmation();
                }

                //get_work_details_in_shared_preference(latitude,longitude,state,workerName,workerImage,workerPhoneNo,jobStatus,jobTitle,work_status,jobId,jobCost,customerPay,type);

                break;
            case "Job Amount":
                SetTexts_Of_All_Views();
                jobcost_confirmation();


                break;

            case "Job Start":
                Log.d("TTT", "Start");
                UnregisterReceiverAndStopService();
                SetTexts_Of_All_Views();

                //get_work_details_in_shared_preference(latitude,longitude,state,workerName,workerImage,workerPhoneNo,jobStatus,jobTitle,work_status,jobId,jobCost,customerPay,type);

                break;

            case "Job Complete":
                Log.d("TTT", "Completed");
                SetTexts_Of_All_Views();
                UnregisterReceiverAndStopService();

                jobCost = Jobdetails_SharedPreference.getjobCost(Current_worker.this);
                customerPay = Jobdetails_SharedPreference.getcustomerPay(Current_worker.this);

                //get_work_details_in_shared_preference(latitude,longitude,state,workerName,workerImage,workerPhoneNo,jobStatus,jobTitle,work_status,jobId,jobCost,customerPay,type);

                Log.d("JobDetails", "latitude: " + Jobdetails_SharedPreference.getlongitude(Current_worker.this) + " longitude: " + Jobdetails_SharedPreference.getlongitude(Current_worker.this));
                i = new Intent(Current_worker.this, Current_worker_bill.class);
                i.putExtra("jobId", jobId);
                i.putExtra("workerName", workerName);
                i.putExtra("workerImage", workerImage);
                i.putExtra("jobTitle", jobTitle);
                i.putExtra("jobCost", jobCost);
                i.putExtra("customerPay", customerPay);
                startActivity(i);
                finish();
                break;
            case "Job Cancel":
                UnregisterReceiverAndStopService();
                showAlertDialogForCancelJob();
                break;
        }

    }


//2018-02-12 19:18:00

    public void showAlertDialogForCancelJob() {
        android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AppTheme));
        dialogBuilder.setTitle("Alert");
        dialogBuilder.setMessage("Job Cancelled By Customer");
        dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                i = new Intent(Current_worker.this, CategoryActivity.class);
                startActivity(i);
                finish();
            }
        });
        android.app.AlertDialog b = dialogBuilder.create();
        b.show();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        SelectState();
    }

    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
    }

    void RegisterReceiverAndStartService() {

        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MyService.MY_ACTION);
        registerReceiver(myReceiver, intentFilter);
        //Start our own service
        i = new Intent(Current_worker.this, MyService.class);
        i.putExtra("phoneNo", workerPhoneNo); //Optional parameters
        startService(i);

    }

    public void UnregisterReceiverAndStopService() {
        if (myReceiver != null) {
            unregisterReceiver(myReceiver);
            stopService(new Intent(Current_worker.this, MyService.class));
            myReceiver = null;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.tv_cancel_job:
                stopService(new Intent(MyService.MY_ACTION));
                cancellationReson();
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (longitude != 0 && latitude != 0) {
            setMarker(latitude, longitude);
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        UnregisterReceiverAndStopService();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        UnregisterReceiverAndStopService();
    }

    public void setMarker(Double mLat, Double mLng) {

        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.add_marker);

        //Place current location marker
        latLng = new LatLng(mLat, mLng);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(icon);
        if (mMarker == null)
            mMarker = mMap.addMarker(markerOptions);
        else
            mMarker.setPosition(latLng);
        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18));
    }

    private class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context arg0, Intent arg1) {
            // TODO Auto-generated method stub

            latitude = arg1.getDoubleExtra("latitudee", 0);
            longitude = arg1.getDoubleExtra("longitudee", 1);

            if (latitude != 0 & longitude != 0) {
                setMarker(latitude, longitude);
            }


        }

    }

    public void jobcost_confirmation() {


        SharedPreferences preferences = getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putString("confirm_amount", "true");
        editor.commit();

        final AlertDialog.Builder mBuilder = new AlertDialog.Builder(Current_worker.this);
        View mView = getLayoutInflater().inflate(R.layout.dialog_jobamount, null);
        mBuilder.setCancelable(false);


        Button btn_accept_amount = (Button) mView.findViewById(R.id.btn_accept_cost);
        Button btn_cancel_job = (Button) mView.findViewById(R.id.btn_cancel_job);
        TextView tv_jobCost = mView.findViewById(R.id.tv_jobCost);
        String cost = Jobdetails_SharedPreference.getjobCost(getApplicationContext());
        tv_jobCost.setText("" + cost);
        mBuilder.setView(mView);

        dialog = mBuilder.create();
        dialog.show();
        btn_cancel_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                stopService(new Intent(MyService.MY_ACTION));
                cancellationReson();
            }
        });
        btn_accept_amount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                accept_jobCost();
                editor.putString("confirm_amount", "false");
                editor.commit();
            }
        });

    }

    public void accept_jobCost() {
        Log.d("JOBID", ConfigURL.getjobId(Current_worker.this));
        AndroidNetworking.post(ConfigURL.URL_CONFIRM_JOB_AMOUNT)
                .addBodyParameter("jobId", ConfigURL.getjobId(Current_worker.this))
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            if (!response.getBoolean("error")) {

                                //Toast.makeText(Current_worker.this, "gaya", Toast.LENGTH_LONG).show();
                                dialog.cancel();

                            } else if (response.getBoolean("error")) {
                                Toast.makeText(Current_worker.this, "error", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        Toast.makeText(Current_worker.this, "" + error, Toast.LENGTH_LONG).show();
                    }
                });
    }

    public void cancellationReson() {

        final AlertDialog.Builder mBuilder = new AlertDialog.Builder(Current_worker.this);
        View mView = getLayoutInflater().inflate(R.layout.dialog_cancellation_reason, null);
        final RadioButton rb_13 = (RadioButton) mView.findViewById(R.id.rb_13);
        final RadioButton rb_14 = (RadioButton) mView.findViewById(R.id.rb_14);
        final RadioButton rb_15 = (RadioButton) mView.findViewById(R.id.rb_15);
        final RadioButton rb_16 = (RadioButton) mView.findViewById(R.id.rb_16);
        final RadioButton rb_17 = (RadioButton) mView.findViewById(R.id.rb_17);
        final RadioButton rb_18 = (RadioButton) mView.findViewById(R.id.rb_18);


        Button btn_exit = (Button) mView.findViewById(R.id.btn_exit);
        Button btn_cancel_jobs = (Button) mView.findViewById(R.id.btn_cancel_job);

        mBuilder.setView(mView);

        final AlertDialog dialog = mBuilder.create();
        dialog.show();
        btn_cancel_jobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String radioButtonChecked = "";
                if (rb_13.isChecked()) {
                    radioButtonChecked = "13";
                } else if (rb_14.isChecked()) {
                    radioButtonChecked = "14";
                } else if (rb_15.isChecked()) {
                    radioButtonChecked = "15";
                } else if (rb_16.isChecked()) {
                    radioButtonChecked = "16";
                } else if (rb_17.isChecked()) {
                    radioButtonChecked = "17";
                } else if (rb_18.isChecked()) {
                    radioButtonChecked = "18";
                }
                Log.d("RSS", "checked : " + radioButtonChecked + "\nJobid  : " + Jobdetails_SharedPreference.getjobId(Current_worker.this));
                isError = ConfigURL.JobCancel(Current_worker.this, Jobdetails_SharedPreference.getjobId(Current_worker.this), radioButtonChecked);
                if (isError)
                    return;

                SharedPreferences preferences = getSharedPreferences("PREFRENCE", 0);
                preferences.edit().remove("latitude");
                preferences.edit().remove("longitude");
                preferences.edit().remove("workerName");
                preferences.edit().remove("workerImage");
                preferences.edit().remove("workerPhoneNo");
                preferences.edit().remove("jobStatus");
                preferences.edit().remove("jobTitle");
                preferences.edit().remove("jobId");
                preferences.edit().remove("jobCost");
                preferences.edit().remove("customerPay");
                preferences.edit().remove("type");
                preferences.edit().commit();
                dialog.cancel();

                i = new Intent(Current_worker.this, CategoryActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });
        btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

    }

}
