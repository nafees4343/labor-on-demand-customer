package com.lod.myapplication_user.My_orders;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.lod.myapplication_user.Adapters.Adapter_order_cancel;
import com.lod.myapplication_user.Models.Customer_cancelled_orders;
import com.lod.myapplication_user.R;
import com.lod.myapplication_user.Schedule_jobs.customer_schedulejobs;
import com.lod.myapplication_user.UtilsAndUrls.ConfigURL;
import com.lod.myapplication_user.UtilsAndUrls.Wifi_check;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class orders_cancelled extends Fragment  {

    ArrayList<Customer_cancelled_orders> data;
    RecyclerView rv;
    private boolean _hasLoadedOnce= false; // your boolean field
    TextView tv_no_canceljobs;


    private OnFragmentInteractionListener mListener;

    public orders_cancelled() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_orders_cancelled, container, false);

        tv_no_canceljobs = rootView.findViewById(R.id.tv_no_canceljobs);

        rv = (RecyclerView) rootView.findViewById(R.id.rv_order_cancel);
        rv.setHasFixedSize(true);

        if (new Wifi_check(getActivity()).isNetworkAvailable()) {
            get_order_cancelled_data();

        } else {
            Snackbar.make(getActivity().findViewById(android.R.id.content), "Internet not connected", Snackbar.LENGTH_SHORT).show();
        }

        return rootView;


    }

    public void get_order_cancelled_data()
    {
        AndroidNetworking.get(ConfigURL.URL_GET_PERSON_ORDERS)
                .addQueryParameter("customerMobileNo", ConfigURL.getMobileNumber(getActivity()))
                .addQueryParameter("jobStatus", "CANCELED")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        data = new ArrayList<>();

                        try {
                            JSONArray jsonArray = response.getJSONArray("customerJobs");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                String jobTitle,personName,canceledBy,jobCreationTime;

                                Customer_cancelled_orders customer_cancelled_orders = new Customer_cancelled_orders(jsonArray.getJSONObject(i));

                                jobTitle = customer_cancelled_orders.getJobTitle();
                                personName = customer_cancelled_orders.getPersonName();
                                canceledBy = customer_cancelled_orders.getCanceledBy();
                                jobCreationTime = customer_cancelled_orders.getJobCreationTime();

                                Log.v("Tele ","jobtitle : "+jobTitle+" person name : "+personName+" canceled by : "+canceledBy+"jobCreationTime : "+jobCreationTime);
                                Customer_cancelled_orders obj = new Customer_cancelled_orders( jobTitle,personName,canceledBy,jobCreationTime);

                                data.add(obj);
                            }

                            Adapter_order_cancel adapter = new Adapter_order_cancel(getActivity(),data);
                            rv.setAdapter(adapter);
                            LinearLayoutManager llm = new LinearLayoutManager(getActivity());
                            rv.setLayoutManager(llm);

                            customer_schedulejobs.Chk_arraylistSize(data,tv_no_canceljobs);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(getActivity(), "" + anError, Toast.LENGTH_LONG).show();

                    }
                });
    }



    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
