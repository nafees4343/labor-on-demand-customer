package com.lod.myapplication_user.My_orders;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.lod.myapplication_user.Adapters.Adapter_order_completed;
import com.lod.myapplication_user.Models.Customer_completed_orders;
import com.lod.myapplication_user.OrderDetailPage;
import com.lod.myapplication_user.R;
import com.lod.myapplication_user.Schedule_jobs.customer_schedulejobs;
import com.lod.myapplication_user.UtilsAndUrls.ConfigURL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class orders_completed extends Fragment implements Adapter_order_completed.ItemClickListener {


    ArrayList<Customer_completed_orders> data;
    ProgressDialog progressDialog;
    RecyclerView rv;
    private orders_cancelled.OnFragmentInteractionListener mListener;
    private boolean _hasLoadedOnce= false; // your boolean field
    TextView tv_no_completedjobs;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_orders_completed, container, false);

        tv_no_completedjobs = rootView.findViewById(R.id.tv_no_completedjobs);

        rv = (RecyclerView) rootView.findViewById(R.id.rv_order_completed);
        rv.setHasFixedSize(true);

        //GET COMPLETED Orders
        get_order_completed_data();

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof orders_cancelled.OnFragmentInteractionListener) {
            mListener = (orders_cancelled.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void get_order_completed_data() {

        AndroidNetworking.get(ConfigURL.URL_GET_PERSON_ORDERS)
                .addQueryParameter("customerMobileNo", ConfigURL.getMobileNumber(getActivity()))
                .addQueryParameter("jobStatus", "COMPLETED")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        data = new ArrayList<>();

                        try {
                            JSONArray jsonArray = response.getJSONArray("customerJobs");
                            Log.v("ResponseAray",""+jsonArray);
                            for (int i = 0; i < jsonArray.length(); i++) {

                                String RattingByWorker, RattingByCustomer, personName, jobStatus, jobAddress, jobBookingStatus, jobCost, jobHours, jobStartTime, jobCompleteTime, jobTitle;

                                Customer_completed_orders customer_ordersCompleted = new Customer_completed_orders(jsonArray.getJSONObject(i));
                                personName = customer_ordersCompleted.getPersonName();
                                jobStatus = customer_ordersCompleted.getJobStatus();
                                jobAddress = customer_ordersCompleted.getJobAddress();
                                jobBookingStatus = customer_ordersCompleted.getJobBookingStatus();
                                jobCost = customer_ordersCompleted.getJobCost();
                                jobHours = customer_ordersCompleted.getJobHours();
                                jobStartTime = customer_ordersCompleted.getJobStartTime();
                                jobCompleteTime = customer_ordersCompleted.getJobCompleteTime();
                                jobTitle = customer_ordersCompleted.getJobTitle();
                                RattingByCustomer = customer_ordersCompleted.getRattingByCustomer();
                                RattingByWorker = customer_ordersCompleted.getRattingByWorker();

                                Log.v("DATA:","person " + personName + "\n jobStatus    "+ jobStatus
                                        + "  \njobAddress    " + jobAddress  + "   \njob Booking Satus  " + jobBookingStatus + " \n jobCOst  "+ jobCost
                                        + " \n jobStarTime "  + jobStartTime  + "  \n jobCompleteTime  "  +jobCompleteTime
                                );

                                Customer_completed_orders obj = new Customer_completed_orders(personName, jobStatus, jobAddress, jobBookingStatus, jobCost, jobHours, jobStartTime, jobCompleteTime, jobTitle,RattingByWorker,RattingByCustomer);
                                data.add(obj);

                            }

                            Adapter_order_completed adapter = new Adapter_order_completed(getActivity(), data);
                            rv.setAdapter(adapter);
                            adapter.setClickListener(orders_completed.this);

                            customer_schedulejobs.Chk_arraylistSize(data,tv_no_completedjobs);

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(getActivity(), "" + anError, Toast.LENGTH_LONG).show();
                    }
                });
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
    }


    @Override
    public void onClick(View view, int position) {
        Bundle bundle = new Bundle();
        Intent i = new Intent(getActivity(), OrderDetailPage.class);
        i.putExtra("personName",  data.get(position).getPersonName());
        i.putExtra("jobStatus", data.get(position).getJobStatus());
        i.putExtra("jobAddress", data.get(position).getJobAddress());
        i.putExtra("jobBookingStatus", data.get(position).getJobBookingStatus());
        i.putExtra("jobCost", data.get(position).getJobCost());
        i.putExtra("jobHours", data.get(position).getJobHours());
        i.putExtra("jobStartTime", data.get(position).getJobStartTime());
        i.putExtra("jobCompleteTime", data.get(position).getJobCompleteTime());
        i.putExtra("jobTitle", data.get(position).getJobTitle());
        i.putExtra("RattingByCustomer", data.get(position).getRattingByCustomer());
        i.putExtra("RattingByWorker", data.get(position).getRattingByWorker());
        i.putExtras(bundle);
        startActivity(i);

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(isFragmentVisible_);


        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (!isFragmentVisible_ && !_hasLoadedOnce) {
                //run your async task here since the user has just focused on your fragment
                _hasLoadedOnce = true;
            }
        }
    }




}
