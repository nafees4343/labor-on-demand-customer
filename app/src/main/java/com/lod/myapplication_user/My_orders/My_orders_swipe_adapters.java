package com.lod.myapplication_user.My_orders;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.lod.myapplication_user.Schedule_jobs.customer_schedulejobs;

/**
 * Created by Admin on 9/23/2017.
 */

public class My_orders_swipe_adapters extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public My_orders_swipe_adapters(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                customer_schedulejobs tab1 = new customer_schedulejobs();
                return tab1;
            case 1:
                orders_cancelled tab2 = new orders_cancelled();
                return tab2;
            case 2:
                orders_completed tab3 = new orders_completed();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

}