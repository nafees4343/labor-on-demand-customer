package com.lod.myapplication_user;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.lod.myapplication_user.Models.Customer_completed_orders;

public class OrderDetailPage extends AppCompatActivity {
    Customer_completed_orders customer_ordersCompleted;
    TextView tv_order_complete_customer_address,tv_order_complete_category,tv_order_complete_worker_name,
    tv_order_complete_total_time,tv_order_complete_start_date,tv_order_complete_end_date,tv_order_complete_total_cost,tv_order_cancelled_by;
    Float rt_by_worker,rt_by_customer;
    String address,category,worker_name,total_time,start_date,end_date,cost,job_status,jobcanceledBy,RattingByCustomer,RattingByWorker;
    RatingBar rt_bar_by_customer,rt_bar_by_partner;
    LinearLayout layout_order_complete;
    LinearLayout layout_order_cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail_page);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_order_detail);
        toolbar.setTitle("Order detail");
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                onBackPressed();
            }
        });

        register_views();
        getBundle();

    }

    public void register_views(){

        tv_order_complete_category = (TextView) findViewById(R.id.tv_order_complete_category);
        tv_order_complete_worker_name = (TextView) findViewById(R.id.tv_order_complete_worker_name);
        tv_order_complete_total_time = (TextView) findViewById(R.id.tv_order_complete_totalTime);
        tv_order_complete_start_date = (TextView) findViewById(R.id.tv_order_complete_start_date);
        tv_order_complete_end_date = (TextView) findViewById(R.id.tv_order_complete_end_date);
        tv_order_complete_total_cost = (TextView) findViewById(R.id.tv_order_complete_total_cost);
        rt_bar_by_customer = (RatingBar) findViewById(R.id.rb_by_Customer);
        rt_bar_by_partner = (RatingBar) findViewById(R.id.rb_by_Partner);
        layout_order_complete = (LinearLayout) findViewById(R.id.layout_order_complete);

    }

    public void getBundle() {

        Bundle bundle;
        bundle = this.getIntent().getExtras();
        if(bundle!=null) {
            address = (String) bundle.get("jobAddress");
            category = (String) bundle.get("jobTitle");
            worker_name = (String) bundle.get("personName");
            total_time = (String) bundle.get("jobTime");
            start_date = (String) bundle.get("jobStartTime");
            end_date = (String) bundle.get("jobCompleteTime");
            cost = (String) bundle.get("jobCost");
            job_status = (String) bundle.get("jobStatus");
            RattingByCustomer = (String) bundle.get("RattingByCustomer");
            RattingByWorker =(String) bundle.get("RattingByWorker");
        }

        try {

            layout_order_complete.setVisibility(View.VISIBLE);
            if(RattingByWorker!=null && RattingByCustomer!=null){
                rt_bar_by_partner.setRating(Float.parseFloat(RattingByWorker));
                rt_bar_by_customer.setRating(Float.parseFloat(RattingByCustomer));
            }
        }
        /*if(job_status.equals("CANCELED")) {
            layout_order_cancel.setVisibility(View.VISIBLE);
            tv_order_cancelled_by.setText(jobcanceledBy);
        }*/

     catch (NumberFormatException e) {

    }
        tv_order_complete_category.setText(category);
        tv_order_complete_worker_name.setText(worker_name);
        tv_order_complete_start_date.setText(start_date);
        tv_order_complete_end_date.setText(end_date);
        tv_order_complete_total_cost.setText(cost);


    }
}
