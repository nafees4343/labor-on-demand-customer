package com.lod.myapplication_user.Notifications;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.lod.myapplication_user.Current_worker;
import com.lod.myapplication_user.R;
import com.lod.myapplication_user.UtilsAndUrls.Jobdetails_SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    SharedPreferences.Editor mEditor;
    SharedPreferences mpref;

    private static final String TAG = "FCM Service";
    public String personName;
    public String title;
    public String notifyCode;

    //******************************
    String amount ;
    String latitude ;
    String longitude ;
    String Time_job_received ;
    String workerName ;
    String workerImage ;
    String workerMobileNo ;
    String jobTitle ;
    String jobStatus ;
    String workerRattings ;
    String jobId ;
    String jobCost ;
    String customerPay ;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getData().toString());
        try {
            JSONObject notificationObject = new JSONObject(remoteMessage.getData().toString());
            addNotification(notificationObject.getJSONObject("data"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private  void buildNotification(JSONObject notificationObject) throws JSONException {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("" + notificationObject.getString("title"))
                        .setContentText("" + notificationObject.getString("message"));
        builder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());



    }
    private void addNotification(JSONObject notificationObject) throws JSONException {

        //Toast.makeText(MyFirebaseMessagingService.this, "latitude:" + latitude + "long:" + longitude + "title:" + title, Toast.LENGTH_LONG).show();

        Intent notificationIntent ;

        notifyCode = notificationObject.getString("type");

        // Log.d("NParam", "personId" + personId + "personName" + personName + "workerImage" + workerImage + "latitude" + latitude + "longitude" + longitude+"personMobileNo"+personMobileNo);

        Log.d("BBB", "lat: " + latitude + "lng: " + longitude + "" + personName + " " + notifyCode);

        switch (notifyCode) {
            case "Job Recived":
                Log.d("NNNN", "stoppp");

               /* mpref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                mEditor = mpref.edit();
                 Intent i = new Intent(getApplicationContext(),Timer_Service.class);
                 stopService(i);
                 //mEditor.putString("data", "").commit();
                 mEditor.remove("data");
                 mEditor.clear().commit();*/

                 latitude = notificationObject.getString("latitude");
                 longitude = notificationObject.getString("longitude");
                 Time_job_received = notificationObject.getString("timestamp");
                 workerName = notificationObject.getString("workerName");
                 workerImage = notificationObject.getString("image");
                 workerMobileNo = notificationObject.getString("workerMobileNo");
                 jobTitle = notificationObject.getString("title");
                 jobStatus = notificationObject.getString("jobStatus");
                 workerRattings = notificationObject.getString("workerRattings");
                 jobId = notificationObject.getString("jobId");
                 jobCost = notificationObject.getString("jobCost");
                 customerPay = notificationObject.getString("customerPay");

                Log.d("NNN",""+notifyCode);

                Jobdetails_SharedPreference.get_work_details_in_shared_preference(getApplicationContext(),latitude, longitude, workerName,  workerImage, workerMobileNo,  jobStatus,  jobTitle, jobId, jobCost, customerPay, notifyCode);

                notificationIntent = new Intent(this, Current_worker.class);
                notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT |Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(notificationIntent);
                break;



            case "Job Arrived":
                //Jobdetails_SharedPreference.get_work_details_in_shared_preference(getApplicationContext(),latitude, longitude, workerName,  workerImage, workerMobileNo,  jobStatus,  jobTitle, jobId, jobCost, customerPay, notifyCode);
                Log.d("NNN",""+notifyCode);
                notificationIntent = new Intent(this, Current_worker.class);
                Jobdetails_SharedPreference.set_type_in_shared_preference(getApplicationContext(),notifyCode);
                notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT |Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(notificationIntent);
                break;

            case "Job Amount":
                amount = notificationObject.getString("jobCost");
                Jobdetails_SharedPreference.save_jobCost(getApplicationContext(),amount);
                Jobdetails_SharedPreference.set_type_in_shared_preference(getApplicationContext(),notifyCode);
                notificationIntent = new Intent(this, Current_worker.class);
                notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT |Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(notificationIntent);
                break;

            case "Job Start":
                Log.d("NNN",""+notifyCode);
                //Jobdetails_SharedPreference.get_work_details_in_shared_preference(getApplicationContext(),latitude, longitude, workerName,  workerImage, workerMobileNo,  jobStatus,  jobTitle, jobId, jobCost, customerPay, notifyCode);
                Jobdetails_SharedPreference.set_type_in_shared_preference(getApplicationContext(),notifyCode);
                notificationIntent = new Intent(this, Current_worker.class);
                notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT |Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(notificationIntent);
                // visible
                break;

            case "Job Complete":
                Log.d("NNN",""+notifyCode);
                //Jobdetails_SharedPreference.get_work_details_in_shared_preference(getApplicationContext(),latitude, longitude, workerName,  workerImage, workerMobileNo,  jobStatus,  jobTitle, jobId, jobCost, customerPay, notifyCode);
                Jobdetails_SharedPreference.set_type_in_shared_preference(getApplicationContext(),notifyCode);


                notificationIntent = new Intent(this, Current_worker.class);
                 jobCost = notificationObject.getString("jobCost");
                 customerPay = notificationObject.getString("customerPay");

                Jobdetails_SharedPreference.set_Job_cost_in_shared_preference(getApplicationContext(),jobCost);
                Jobdetails_SharedPreference.set_customer_Pay_in_shared_preference(getApplicationContext(),customerPay);

                notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT |Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(notificationIntent);
                //
                break;

            case "Job Cancel":
                notificationIntent = new Intent(this, Current_worker.class);
                Jobdetails_SharedPreference.set_type_in_shared_preference(getApplicationContext(),notifyCode);
                notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT |Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(notificationIntent);
                break;

            case "Promotion":
                //
                buildNotification(notificationObject);
                break;
        }
    }



}
