package com.lod.myapplication_user.Notifications;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.lod.myapplication_user.R;

import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button bt_update;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bt_update = (Button) findViewById(R.id.bt_gcm);
        bt_update.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.bt_gcm:
                sendRegistrationToServer(FirebaseInstanceId.getInstance().getToken());
                Log.d("FCM Registration Token",""+FirebaseInstanceId.getInstance().getToken());
                break;
        }
    }
    public  void sendRegistrationToServer(String token) {
        String URL = "http://www.itehadmotors.com/qasimWork/laborondemandwebservice/v1/fcm";

        AndroidNetworking.initialize(getApplicationContext());
        AndroidNetworking.post(URL)
                .addBodyParameter("apiKey", token)
                .addBodyParameter("workerId", "123")
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.v("FIREBASE:result",""+response);
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.v("FIREBASE:error",""+error);
                    }
                });

    }

}
