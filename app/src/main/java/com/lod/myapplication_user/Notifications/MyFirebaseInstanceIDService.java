package com.lod.myapplication_user.Notifications;

import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.lod.myapplication_user.UtilsAndUrls.ConfigURL;

import org.json.JSONObject;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {


    private static final String TAG = "FirebaseIDService";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // TODO: Implement this method to send any registration to your app's servers.
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        String URL = "http://www.itehadmotors.com/qasimWork/laborondemandwebservice/v1/insertfcmcustomer";


        AndroidNetworking.initialize(getApplicationContext());
        AndroidNetworking.post(URL)
                .addBodyParameter("apiKey", token)
                .addBodyParameter("customerMobileNo", ConfigURL.getMobileNumber(this))
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.v("FIREBASE:result",""+response);
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.v("FIREBASE:error",""+error);
                    }
                });
    }

}
