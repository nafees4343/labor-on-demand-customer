package com.lod.myapplication_user.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lod.myapplication_user.Models.Customer_completed_orders;
import com.lod.myapplication_user.R;

import java.util.ArrayList;


public class Adapter_order_completed extends RecyclerView.Adapter< Adapter_order_completed.MyViewHolder> {

    private ArrayList<Customer_completed_orders> arrayList ;
    private Context acontext;
    private Adapter_order_completed.ItemClickListener clickListener;

    public interface ItemClickListener {
        void onClick(View view, int position);
    }

    public interface OnItemClickListener {
        public void onClick(View view, int position);
    }

    public void setClickListener(Adapter_order_completed.ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public  class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public CardView mCardView;
        public TextView tv_order_completed_cost,tv_order_completed_name,tv_order_completed_category,tv_order_completed_end_date;
        public MyViewHolder(View v){
            super(v);

            mCardView = (CardView) v.findViewById(R.id.card_view_order_completed);
            tv_order_completed_name = (TextView) v.findViewById(R.id.tv_order_completed_person_name);
            tv_order_completed_category = (TextView) v.findViewById(R.id.tv_order_completed_category);
            tv_order_completed_cost = (TextView) v.findViewById(R.id.tv_order_completed_cost);
            tv_order_completed_end_date = (TextView) v.findViewById(R.id.tv_order_completed_end_date);
            itemView.setOnClickListener(this); // bind the listener


        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());

        }
    }


    public Adapter_order_completed(Context context,ArrayList<Customer_completed_orders> arrayList) {
        this.arrayList = arrayList;
        acontext = context;
    }


    @Override
    public Adapter_order_completed.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_order_completed, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(Adapter_order_completed.MyViewHolder holder, int position){

        Customer_completed_orders current = arrayList.get(position);
        holder.tv_order_completed_category.setText(current.getJobTitle());
        holder.tv_order_completed_name.setText(current.getPersonName());
        holder.tv_order_completed_cost.setText(current.getJobCost());
        holder.tv_order_completed_end_date.setText(current.getJobCompleteTime());
    }

    @Override
    public int getItemCount() { return arrayList.size(); }
}
