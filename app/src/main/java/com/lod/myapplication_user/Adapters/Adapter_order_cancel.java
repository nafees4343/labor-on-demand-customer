package com.lod.myapplication_user.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lod.myapplication_user.Models.Customer_cancelled_orders;
import com.lod.myapplication_user.R;

import java.util.ArrayList;

/**
 * Created by nfs
 */
public class Adapter_order_cancel extends RecyclerView.Adapter<Adapter_order_cancel.MyViewHolder> {

    private ArrayList<Customer_cancelled_orders> arrayList ;
    Context acontext;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public CardView mCardView;
        public TextView mTextView;
        public TextView tv_order_cancel_category,tv_order_cancel_person_name,tv_cancelby,tv_jobcreationtime;

        public MyViewHolder(View v){
            super(v);

            mCardView = (CardView) v.findViewById(R.id.card_view_order_cancel);
            tv_order_cancel_person_name= (TextView) v.findViewById(R.id.tv_order_cancel_person_name);
            tv_order_cancel_category = (TextView) v.findViewById(R.id.tv_order_cancel_category);
            tv_cancelby = (TextView) v.findViewById(R.id.tv_order_cancelby);
            tv_jobcreationtime = (TextView) v.findViewById(R.id.tv_order_cancel_end_date);


        }


    }

    public Adapter_order_cancel(Context context, ArrayList<Customer_cancelled_orders> arrayList) {
        this.arrayList = arrayList;
        acontext = context;
    }
    @Override
    public Adapter_order_cancel.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_order_cancel, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position){

        Customer_cancelled_orders current = arrayList.get(position);
        Log.v("Adcancel","current.getPersonName : "+ current.getPersonName()+" current.getJobTitle : "+current.getJobTitle()+
                "current.getCanceledBy : "+current.getCanceledBy()+" current.getJobCreationTime() : "+current.getJobCreationTime());
        holder.tv_order_cancel_person_name.setText(current.getPersonName());
        holder.tv_order_cancel_category.setText(current.getJobTitle());
        holder.tv_cancelby.setText(current.getCanceledBy());
        holder.tv_jobcreationtime.setText(current.getJobCreationTime());
    }

    @Override
    public int getItemCount() { return arrayList.size(); }

}
