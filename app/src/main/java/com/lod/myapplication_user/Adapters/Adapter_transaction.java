package com.lod.myapplication_user.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lod.myapplication_user.Models.Transaction;
import com.lod.myapplication_user.R;

import java.util.ArrayList;

/**
 * Created by nfs
 */
public class Adapter_transaction extends RecyclerView.Adapter< Adapter_transaction.MyViewHolder> {

    private ArrayList<Transaction> arrayList;
    private Context acontext;


    public static class MyViewHolder extends RecyclerView.ViewHolder{

        public CardView mCardView;
        TextView tv_jobId,tv_Transfer,tv_Amount,tv_time,tv_Balance;


        public MyViewHolder(View v) {
            super(v);

            mCardView = (CardView) v.findViewById(R.id.card_transaction);
            tv_jobId = (TextView) v.findViewById(R.id.tv_jobId);
            tv_Transfer = (TextView) v.findViewById(R.id.tv_Transfer);
            tv_Amount = (TextView) v.findViewById(R.id.tv_Amount);
            tv_time = (TextView) v.findViewById(R.id.tv_time);
            tv_Balance = (TextView) v.findViewById(R.id.tv_Balance);
        }
    }


    public Adapter_transaction(Context context, ArrayList<Transaction> arrayList){
        this.arrayList = arrayList;
        this.acontext = context;
    }

    @Override
    public Adapter_transaction.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_transaction, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(Adapter_transaction.MyViewHolder holder, int position) {
        Transaction current = arrayList.get(position);
        holder.tv_jobId.setText(current.getJobId());
        holder.tv_Transfer.setText(current.getTransfer());
        holder.tv_Amount.setText(current.getAmount());
        holder.tv_time.setText(current.getUpdateTime());
        holder.tv_Balance.setText(current.getBalance());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


}
