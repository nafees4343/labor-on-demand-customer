package com.lod.myapplication_user.Network;

import android.support.multidex.MultiDexApplication;


public class StatusRegister extends MultiDexApplication{

    private static StatusRegister mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
    }

    public static synchronized StatusRegister getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }
}
