package com.lod.myapplication_user;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.lod.myapplication_user.Adapters.Adapter_transaction;
import com.lod.myapplication_user.Models.Transaction;
import com.lod.myapplication_user.Schedule_jobs.customer_schedulejobs;
import com.lod.myapplication_user.UtilsAndUrls.ConfigURL;
import com.lod.myapplication_user.UtilsAndUrls.WalletAmountForCustomer;
import com.lod.myapplication_user.UtilsAndUrls.Wifi_check;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TransactionActivity extends AppCompatActivity {

    ArrayList<Transaction> data;
    RecyclerView rv;
    TextView tv_no_transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_transaction);
        toolbar.setTitle("Transactions");
        setSupportActionBar(toolbar);

        tv_no_transaction = findViewById(R.id.tv_no_transaction);

        rv = (RecyclerView)findViewById(R.id.rv_recycler_view);
        rv.setHasFixedSize(true);


        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                onBackPressed();

            }
        });

        if (new Wifi_check(this).isNetworkAvailable()) {
            ProgressDialogClass.showDottedProgress(TransactionActivity.this,"Please Wait...");
            WalletAmountForCustomer.getWalletAmount(TransactionActivity.this);
            get_order_cancelled_data();
        }
        else
        {
            Snackbar.make(findViewById(android.R.id.content), "Internet not connected", Snackbar.LENGTH_SHORT).show();
        }

    }

    public void get_order_cancelled_data()
    {
        AndroidNetworking.get(ConfigURL.URL_GET_TRANSACTION)
                .addQueryParameter("pMobileNo", ConfigURL.getMobileNumber(TransactionActivity.this))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("SSD","id = "+response);
                        ProgressDialogClass.dismissDottedProgress();
                        data = new ArrayList<>();

                        try {
                            JSONArray jsonArray = response.getJSONArray("customerWallet");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                String jobId,Transfer,Amount,updateTime,Balance;
                                Transaction customer_wallet = new Transaction(jsonArray.getJSONObject(i));
                                jobId = customer_wallet.getJobId();
                                updateTime = customer_wallet.getUpdateTime();
                                Transfer = customer_wallet.getTransfer();
                                Amount = customer_wallet.getAmount();
                                Balance = customer_wallet.getBalance();
                                Log.d("SSD","id = "+jobId+updateTime+Transfer+Amount+Balance);
                                Transaction obj = new Transaction(jobId,Transfer,Amount,updateTime,Balance);

                                data.add(obj);
                            }

                            Adapter_transaction adapter = new Adapter_transaction(TransactionActivity.this,data);
                            rv.setAdapter(adapter);

                            customer_schedulejobs.Chk_arraylistSize(data,tv_no_transaction);


                        } catch (JSONException e) {
                            ProgressDialogClass.dismissDottedProgress();
                            e.printStackTrace();
                            Log.d("SSD","id = " +e.getLocalizedMessage());

                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        ProgressDialogClass.dismissDottedProgress();
                        Toast.makeText(TransactionActivity.this, "" + anError, Toast.LENGTH_LONG).show();
                        Log.d("SSD","id = " +anError
                        );


                    }
                });
        LinearLayoutManager llm = new LinearLayoutManager(TransactionActivity.this);
        rv.setLayoutManager(llm);
    }




}
