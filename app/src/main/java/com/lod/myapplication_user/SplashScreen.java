package com.lod.myapplication_user;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.lod.myapplication_user.Category.CategoryActivity;
import com.lod.myapplication_user.Login.LoginActivity;
import com.lod.myapplication_user.Models.Customer_job_status;
import com.lod.myapplication_user.UtilsAndUrls.ConfigURL;
import com.lod.myapplication_user.UtilsAndUrls.Jobdetails_SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SplashScreen extends Activity {

    Context context;
    public static Activity fa;
    private static int SPLASH_TIME_OUT = 1000;
    String latitude, longitude, type, jobId, state, workerName, workerImage, jobStatus, jobTitle, workerPhoneNo, work_status, operator, jobCost, customerPay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                if (ConfigURL.getLoginState(SplashScreen.this).length() > 0) {
                    checkJobStatus();
                    // close this activity
                } else {
                    Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                    // close this activity
                }

            }
        }, SPLASH_TIME_OUT);
    }

    public void checkJobStatus() {

        AndroidNetworking.get(ConfigURL.URL_CUSTOMER_JOB_STATUS)
                .addQueryParameter("customerMobileNo", ConfigURL.getMobileNumber(this))
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        Log.d("LResponse", "Response: " + response);
                        //ProgressDialogClass.dismissDottedProgress();


                        try {
                            /*Boolean error = false;
                            error = response.getBoolean("error");*/
                            Log.d("AAA", " TRY");

                            JSONArray jsonArray = response.getJSONArray("customerJob");

                            if (jsonArray.length() <= 0) {

                                Log.d("BBB", "empty");
                                Intent intent = new Intent(SplashScreen.this, CategoryActivity.class);
                                startActivity(intent);
                                finish();

                            } else {
                                Log.d("BBB", "not empty");

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    Customer_job_status category = new Customer_job_status(jsonArray.getJSONObject(i));
                                    jobId = category.getJobId();
                                    jobStatus = category.getJobStatus();
                                    workerName = category.getWorkerName();
                                    workerPhoneNo = category.getWorkerPhoneNo();
                                    jobCost = category.getJobCost();
                                    workerImage = category.getWorkerImage();
                                    jobStatus = category.getJobStatus();
                                    jobTitle = category.getJobTitle();
                                    work_status = category.getWork_status();
                                    operator = category.getOperator();
                                    customerPay = category.getCustomerPay();
                                    latitude = category.getLatitude();
                                    longitude = category.getLongitude();

                                }

                                SetJobStatus();

                                Log.d("JB_status", "\ntype: " + type + "\njobStatus: " + jobStatus + "\njob iD" + jobId + "\nwork name: " + workerName + "\nworker Phone no" + workerPhoneNo + "\ncost: " + jobCost+"\nimage: "+workerImage);

                                Jobdetails_SharedPreference.get_work_details_in_shared_preference(getApplicationContext(), latitude, longitude, workerName, workerImage, workerPhoneNo, jobStatus, jobTitle, jobId, jobCost, customerPay, jobStatus);

                                if (Jobdetails_SharedPreference.getjobStatus(SplashScreen.this).equals("COMPLETED")) {
                                    Intent intent = new Intent(SplashScreen.this, Current_worker_bill.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Intent intent = new Intent(SplashScreen.this, Current_worker.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        //ProgressDialogClass.dismissDottedProgress();
                        Toast.makeText(SplashScreen.this, "" + anError, Toast.LENGTH_LONG).show();

                    }
                });
    }

    public void SetJobStatus() {

        Log.d("jb", "" + jobStatus);

        if (jobStatus != null) {

            switch (jobStatus + "") {

                case "ACTIVE":
                    jobStatus = "Job Recived";
                    break;
                case "ARRIVED":
                    jobStatus = "Job Arrived";
                    break;
                case "START":
                    jobStatus = "Job Start";
                    break;
                case "COMPLETED":
                    jobStatus = "Job Complete";
                    break;

            }
        }
    }

}