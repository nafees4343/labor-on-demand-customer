package com.lod.myapplication_user.Category;

/**
 * Created by Admin on 10/17/2017.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lod.myapplication_user.Models.Category;
import com.lod.myapplication_user.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.lod.myapplication_user.R.id.iv_category;


public class Adapter_Category_list extends BaseAdapter {

    private static LayoutInflater inflater = null;
    Context context;
    ArrayList<Category> data;

    public Adapter_Category_list(CategoryActivity mainActivity, ArrayList<Category> data) {
        // TODO Auto-generated constructor stub
        this.data = data;
        context = mainActivity;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder = new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.item_category_list, null);
        holder.tv_category = (TextView) rowView.findViewById(R.id.textView_category);
        holder.iv_categoryImage = (ImageView) rowView.findViewById(iv_category);


        Category current = data.get(position);
        holder.tv_category.setText(current.getCategoryname());
        if (!current.getCategoryImage().equals("")){
            Picasso.with(context)
                    .load(current.getCategoryImage())
                    .fit()
                    .into(holder.iv_categoryImage);
                 }
        return rowView;
    }

    public class Holder {
        TextView tv_category;
        ImageView iv_categoryImage;
    }

}