package com.lod.myapplication_user.Category;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.lod.myapplication_user.Models.Category;
import com.lod.myapplication_user.Nav_drawer_user;
import com.lod.myapplication_user.ProgressDialogClass;
import com.lod.myapplication_user.R;
import com.lod.myapplication_user.UtilsAndUrls.ConfigURL;
import com.lod.myapplication_user.UtilsAndUrls.WalletAmountForCustomer;
import com.lod.myapplication_user.UtilsAndUrls.Wifi_check;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CategoryActivity extends Nav_drawer_user {


    ListView gv;
    ArrayList<Category> data;
    TextView tv_wallet_category;
    static boolean active = false;
    SwipeRefreshLayout sr_category;




    public boolean getactive() {
        return active;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_category, null, false);


        tv_wallet_category = (TextView) findViewById(R.id.tv_wallet_category);

        //inflate your activity layout here!
        drawer.addView(contentView, 1);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_category);

        toolbar.setTitle("Categories");
        setSupportActionBar(toolbar);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        //checkJobStatus();


        // sr_category = (SwipeRefreshLayout)findViewById(R.id.sr_category);
        gv = (ListView) findViewById(R.id.gridView1);


        if (new Wifi_check(this).isNetworkAvailable()) {
            WalletAmountForCustomer.getWalletAmount(CategoryActivity.this);
            get_profile_data();
        } else {
            ProgressDialogClass.dismissDottedProgress();
            Snackbar.make(findViewById(android.R.id.content), "Internet not connected", Snackbar.LENGTH_SHORT).show();
        }

        //.setOnClickListener(this);

        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                // TODO Auto-generated method stub
                // Toast.makeText(CategoryActivity.this, data.get(position)+"", Toast.LENGTH_SHORT).show();
                Bundle bundle = new Bundle();
                Intent intent = new Intent(CategoryActivity.this, Nav_drawer_user.class);
                // Log.v("DDDD" +""," Position " + position + " data " + data.get(position).toString());
                intent.putExtra("category", data.get(position));
                intent.putExtras(bundle);
                startActivity(intent);
                //further cases
            }
        });

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    public void get_profile_data() {
        AndroidNetworking.get(ConfigURL.URL_GET_CATEGORY)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressDialogClass.dismissDottedProgress();
                        data = new ArrayList<>();
                        try {
                            JSONArray jsonArray = response.getJSONArray("category");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                String categoryname, categoryStatus, categoryImage, categoryId;

                                Category category = new Category(jsonArray.getJSONObject(i));
                                categoryId = category.getCategoryId();
                                categoryname = category.getCategoryname();
                                categoryStatus = category.getCategoryStatus();
                                categoryImage = category.getCategoryImage();

                                Category obj = new Category(categoryId, categoryname, categoryStatus, categoryImage);
                                data.add(obj);

                            }

                            Adapter_Category_list adapter = new Adapter_Category_list(CategoryActivity.this, data);
                            gv.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        ProgressDialogClass.dismissDottedProgress();
                        Toast.makeText(CategoryActivity.this, "" + anError, Toast.LENGTH_LONG).show();

                    }
                });
    }


}

